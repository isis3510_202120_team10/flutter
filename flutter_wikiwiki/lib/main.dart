import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/app.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/delivery_cubit.dart';
import 'package:flutter_wikiwiki/src/configuration.dart';
import 'package:flutter_wikiwiki/src/provider/connection.dart';
import 'package:flutter_wikiwiki/src/provider/services.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/auth_repository.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_delivery_repository.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/wait_time_repository.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';
import 'package:flutter_wikiwiki/src/repository/wait_time_repository.dart';

Future<void> main() async {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

    final AnalyticsService analyticsService = AnalyticsService(
      useGoogleAnalytics: kIsWeb == false && kUseFirebaseAnalytics,
    );

    final authCubit = AuthCubit(analyticsService, AuthRepository());
    final deliveryRepository = MyDeliveryRepository();
    final connection = ConnectionStatusSingleton.getInstance();
    connection.initialize();
    final connectionCubit = ConnectionCubit(connection);
    connectionCubit.firstRun();
    final waitTimeRepository = WaitTimeRepository();
    await waitTimeRepository.init();
    final myDeliverCubit = MyDeliveryCubit();
    myDeliverCubit.deliveryRepository = deliveryRepository;

    runApp(
      MultiBlocProvider(
          providers: <BlocProvider<dynamic>>[
            BlocProvider<AuthCubit>(create: (_) => authCubit..init()),
            BlocProvider<ConnectionCubit>(
                create: (_) => connectionCubit..init()),
            BlocProvider<MyDeliveryCubit>(create: (_) => myDeliverCubit)
          ],
          child: RepositoryProvider<MyDeliveryRepositoryBase>(
              create: (_) => deliveryRepository,
              child: RepositoryProvider<WaitTimeRepositoryBase>(
                create: (_) => waitTimeRepository,
                child: MyApp.create(analyticsService: analyticsService),
              ))),
    );
  }, (error, stackTrace) {
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}
