import 'package:equatable/equatable.dart';

class Restaurant extends Equatable {
  final String id;
  final String name;

  final String? logo;

  const Restaurant(this.id, this.name,
      {this.logo});

  @override
  List<Object?> get props => [id];

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'id': id,
      'name': name,
      'logo': logo
    };
  }

  Restaurant.fromFirebaseMap(Map<String, Object?> data, String idNew)
      : id = idNew,
        name = data['name'] as String,
        logo = data['logo'] as String;
}
