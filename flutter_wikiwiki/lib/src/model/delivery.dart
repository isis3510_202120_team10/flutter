import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_wikiwiki/src/model/restaurant.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class MyDelivery extends Equatable {
  final String id;
  final String clientName;
  final String orderID;
  final String phone;
  final String startDateFormated;
  final DocumentReference restaurantID;
  final DocumentReference userID;
  final DateTime startDate;
  int status;
  MyUser? user;
  final bool willPayOnDelivery;
  final Delivery? deliveryOrigin;
  final Delivery? deliveryDestination;
  final double? deliveryPrice;

  final String? deliveryImage;
  DateTime? finishDate;
  Restaurant? restaurant;

  MyDelivery(
      this.id,
      this.clientName,
      this.deliveryDestination,
      this.deliveryOrigin,
      this.orderID,
      this.phone,
      this.restaurantID,
      this.startDate,
      this.status,
      this.userID,
      this.startDateFormated,
      this.willPayOnDelivery,
      {this.finishDate,
      this.deliveryImage,
      this.deliveryPrice,
      this.user, this.restaurant});

  @override
  List<Object?> get props => [id];

  Map<String, Object?> newToFirebaseMap() {
    return <String, Object?>{
      'clientName': clientName,
      'orderID': orderID,
      'phone': phone,
      'user': userID,
      'restaurant': restaurantID,
      'startDate': Timestamp.fromDate(startDate),
      'deliveryOrigin': deliveryOrigin?.toMap(),
      'deliveryDestination': deliveryDestination?.toMap(),
      'status': 0,
      'willPayOnDelivery': willPayOnDelivery,
      'deliveryPrice': deliveryPrice
    };
  }

  Future<void> setUser() async {
    final DocumentSnapshot snapshot = await userID.get();
    final data = snapshot.data();
    user = MyUser.fromFirebaseMap(data as Map<String, Object?>);
  }

  Future<void> setRestaurant() async {
    final DocumentSnapshot snapshot = await restaurantID.get();
    final data = snapshot.data();
    restaurant = Restaurant.fromFirebaseMap(data as Map<String, Object?>, restaurantID.id);
  }

  Map<String, Object?> toFirebaseMap({String? newStatus}) {
    return <String, Object?>{
      'id': id,
      'clientName': clientName,
      'orderID': orderID,
      'phone': phone,
      'user': userID,
      'restaurant': restaurantID,
      'startDate': Timestamp.fromDate(startDate),
      'deliveryOrigin': deliveryOrigin?.toMap(),
      'deliveryDestination': deliveryDestination?.toMap(),
      'deliveryImage': deliveryImage,
      'finishDate': finishDate == null ? null : Timestamp.fromDate(finishDate!),
      'status': newStatus ?? status,
      'willPayOnDelivery': willPayOnDelivery,
      'deliveryPrice': deliveryPrice,
    };
  }

  String getStatusText() {
    switch (status) {
      case 0:
        return 'Picking Up';
      case 1:
        return 'Delivering';
      case 2:
        return 'Finished';
      default:
        return '';
    }
  }

  MyDelivery.fromFirebaseMap(Map<String, dynamic> data, String idNew)
      : id = idNew,
        clientName = data['clientName'] as String,
        orderID = data['orderID'] as String,
        phone = data['phone'] as String,
        userID = data['user'] as DocumentReference,
        restaurantID = data['restaurant'] as DocumentReference,
        startDate = DateTime.parse(
            (data['startDate'] as Timestamp).toDate().toString()),
        startDateFormated = DateFormat('MMM-dd-yyyy H:mm:s')
            .format(DateTime.parse(
                (data['startDate'] as Timestamp).toDate().toString()))
            .toString(),
        deliveryOrigin = Delivery.fromMap(data['deliveryOrigin']),
        deliveryDestination = Delivery.fromMap(data['deliveryDestination']),
        deliveryImage = data['deliveryImage'] as String?,
        finishDate = data['finishDate'] != null
            ? DateTime.parse(
                (data['finishDate'] as Timestamp).toDate().toString())
            : null,
        status = data['status'] as int,
        willPayOnDelivery = data['willPayOnDelivery'] != null
            ? data['willPayOnDelivery'] as bool
            : false,
        deliveryPrice =
            data['deliveryPrice'] != null ? data['deliveryPrice'] as double : 0;
}

class Delivery extends Equatable {
  final String address;
  final GeoPoint coordinate;

  const Delivery(this.address, this.coordinate);

  factory Delivery.fromMap(Map<String, dynamic> map) {
    return Delivery(map['address'], map['coordinate']);
  }

  Map<String, dynamic> toMap() {
    return {'address': address, 'coordinate': coordinate};
  }

  @override
  List<Object?> get props => [address, coordinate];
}
