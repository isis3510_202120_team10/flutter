import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class MyUser extends Equatable {
  final String id;
  final String name;
  final String lastName;
  final String phoneNumber;
  final String role;

  final String? image;
  final DocumentReference? restaurant;

  const MyUser(this.id, this.name, this.lastName, this.phoneNumber, this.role,
      {this.image, this.restaurant});

  @override
  List<Object?> get props => [id];

  Map<String, Object?> toFirebaseMap({String? newImage}) {
    return <String, Object?>{
      'id': id,
      'name': name,
      'lastName': lastName,
      'phoneNumber': phoneNumber,
      'role': role,
      'image': newImage ?? image,
    };
  }

  String getFullName() {
    return name + ' ' + lastName;
  }

  MyUser.fromFirebaseMap(Map<String, Object?> data)
      : id = data['id'] as String,
        name = data['name'] as String,
        lastName = data['lastName'] as String,
        phoneNumber = data['phoneNumber'] as String,
        role = data['role'] as String,
        image = data['image'] as String?,
        restaurant = data['restaurant'] as DocumentReference?;
}
