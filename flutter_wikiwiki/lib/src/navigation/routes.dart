import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wikiwiki/src/ui/intro_screen.dart';
import 'package:flutter_wikiwiki/src/ui/join_detail_order.dart';
import 'package:flutter_wikiwiki/src/ui/navegation_bar.dart';
import 'package:flutter_wikiwiki/src/ui/navegation_bar_owner.dart';
import 'package:flutter_wikiwiki/src/ui/splash_screen.dart';

class Routes {
  static const splash = '/';
  static const intro = '/intro';
  static const home = '/home';
  static const current = '/currentDelivery';
  static const previous = '/previousDeliveries';
  static const profile = '/profile';
  static const driversOwner = '/driversOwner';
  static const ordersOwner = '/ordersOwner';
  static const createOrderOwner = '/createOrderOwner';
  static const profileOwner = '/profileOwner';
  static const orderDetailOwner = '/orderDetailOwner';

  static Route routes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case splash:
        return _buildRoute(SplashScreen.create);
      case intro:
        return _buildRoute(IntroScreen.create);
      case home:
      case current:
        return _buildRouteSelect(NavigationBar.create, 0);
      case previous:
        return _buildRouteSelect(NavigationBar.create, 1);
      case profile:
        return _buildRouteSelect(NavigationBar.create, 2);
      case driversOwner:
        return _buildRouteSelect(NavigationBarOwner.create, 0);
      case ordersOwner:
        return _buildRouteSelect(NavigationBarOwner.create, 1);
      case createOrderOwner:
        return _buildRouteSelect(NavigationBarOwner.create, 2);
      case profileOwner:
        return _buildRouteSelect(NavigationBarOwner.create, 3);
      case orderDetailOwner:
        return _buildRoute(JoinOrderScreen.create);
      default:
        throw Exception('Route does not exists');
    }
  }

  static MaterialPageRoute _buildRoute(Function build) =>
      MaterialPageRoute(builder: (context) => build(context));

  static MaterialPageRoute _buildRouteSelect(Function build, int page) =>
      MaterialPageRoute(builder: (context) => build(context, page));
}
