import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/provider/services.dart';
import 'package:flutter_wikiwiki/src/provider/sharepreferences.dart';
import 'package:flutter_wikiwiki/src/repository/auth_repository.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_user_repository.dart';

const String _method = 'method';
const String _useremail = 'user_email';
const String _useruid = 'user_uid';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepositoryBase _authRepository;
  late StreamSubscription _authSubscription;
  final providerLocal = SharePreferencesProvider();

  final AnalyticsService _analyticsService;

  AuthCubit(
    AnalyticsService analyticsService,
    this._authRepository,
  )   : _analyticsService = analyticsService,
        super(AuthInitialState());

  Future<void> init() async {
    _authSubscription =
        _authRepository.onAuthStateChanged.listen(_authStateChanged);
  }

  void _authStateChanged(AuthUser? user) =>
      user == null ? emit(AuthSignedOut()) : emit(AuthSignedIn(user));

  Future<void> verifyUnlock() async {
    final DateTime? dateVerify = await providerLocal.getLockDate();
    if (dateVerify != null) {
      final int diffVerify = dateVerify.difference(DateTime.now()).inSeconds;
      if (diffVerify <= 0) {
        await providerLocal.resetPinLockUntil();
      }
    }
  }

  Future<void> createUserWithEmailAndPassword(
      String email, String password) async {
    final Future<AuthUser?> auxUser =
        _authRepository.createUserWithEmailAndPassword(email, password);

    final user = await auxUser;
    final MyUser userCreated = MyUser(user!.uid, '', '', '', 'DRIVER');
    MyUserRepository().saveMyUser(userCreated, null);
    _signIn(auxUser, 'sign_up', 'signUpWithEmailAndPassword');
  }

  Future<void> signInWithEmailAndPassword(String email, String password) =>
      _signIn(_authRepository.signInWithEmailAndPassword(email, password),
          'log_in', 'logInWithEmailAndPassword');

  Future<void> signInAnonymously() => _signIn(
      _authRepository.signInAnonymously(), 'sign_up', 'signInAnonymously');

  Future<void> signInWithGoogle() => _signIn(
      _authRepository.signInWithGoogle(), 'sign_in', 'signInWithGoogle');

  Future<void> _signIn(
      Future<AuthUser?> auxUser, String name, String method) async {
    try {
      emit(AuthSigningIn());

      final user = await auxUser;

      if (user == null) {
        emit(AuthError('Unknown error, try again later'));
      } else {
        await _analyticsService.logSignInOrUp(name,
            {_method: method, _useremail: user.email, _useruid: user.uid});
        await providerLocal.resetLockAttempts();
        emit(AuthSignedIn(user));
      }
    } on FirebaseAuthException catch (e) {
      emit(AuthError('Error: ${e.message}'));
    } on PlatformException catch (e) {
      if (e.code == 'network_error') {
        emit(AuthError('Error: Verify your network connection'));
      } else {
        emit(AuthError('Error: ${e.message}'));
      }
    } catch (e) {
      emit(AuthError('Error: ${e.toString()}'));
    }
  }

  Future<void> signOut(AuthUser user) async {
    await _authRepository.signOut();
    await _analyticsService
        .logSignOut({_useremail: user.email, _useruid: user.uid});
    await providerLocal.resetLockAttempts();
    emit(AuthSignedOut());
  }

  @override
  Future<void> close() {
    _authSubscription.cancel();
    return super.close();
  }
}

abstract class AuthState extends Equatable {
  @override
  List<Object?> get props => [];
}

class AuthInitialState extends AuthState {}

class AuthSignedOut extends AuthState {}

class AuthSigningIn extends AuthState {}

class AuthError extends AuthState {
  final String message;

  AuthError(this.message);

  @override
  List<Object?> get props => [message];
}

class AuthSignedIn extends AuthState {
  final AuthUser user;

  AuthSignedIn(this.user);

  @override
  List<Object?> get props => [user];
}
