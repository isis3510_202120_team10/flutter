import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/repository/wait_time_repository.dart';

class WaitTimeCubit extends Cubit<WaitTimeState> {
  final WaitTimeRepositoryBase _waitTimeRepository;
  WaitTimeCubit(this._waitTimeRepository) : super(WaitTimeInitialState());

  Future<void> getWaitTimeRemote(String userId) async {
    try {
      emit(WaitTimeLoadingState());
      final time =  await _waitTimeRepository.getWaitTimeRemote(userId);
      _waitTimeRepository.saveWaitTimeLocal(time, userId);
      emit(WaitTimeCompleteState(time));
    } on Exception catch (e) {
      emit(WaitTimeErrorState(e.toString()));
    }
  }
  Future<void> getWaitTimeLocal(String userId) async{
    try {
      final time = await _waitTimeRepository.getWaitTimeLocal(userId);
      emit(WaitTimeCompleteState(time));
    } on Exception catch (e) {
      emit(WaitTimeErrorState(e.toString()));
    }
  }
}

abstract class WaitTimeState extends Equatable {
  @override
  List<Object> get props => [];
}

class WaitTimeLoadingState extends WaitTimeState {}

class WaitTimeInitialState extends WaitTimeState {}

class WaitTimeCompleteState extends WaitTimeState {
  final String time;

  WaitTimeCompleteState(this.time);

  @override
  List<Object> get props => [time];
}

class WaitTimeErrorState extends WaitTimeState {
  final String message;

  WaitTimeErrorState(this.message);

  @override
  List<Object> get props => [message];
}
