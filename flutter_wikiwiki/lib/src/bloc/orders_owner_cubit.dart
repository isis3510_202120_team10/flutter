import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/provider/sharepreferences.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_user_repository.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';

class OrdersOwnerCubit extends Cubit<OrdersOwnerState> {
  final MyDeliveryRepositoryBase _deliveryRepository;
  final providerLocal = SharePreferencesProvider();

  late MyDelivery _myDelivery;
  late List<MyUser> _deliveryGuys;

  OrdersOwnerCubit(this._deliveryRepository) : super(OrdersOwnerInitialState());

  void listenDeliveriesOwner(Function newState, MyUser myUser) async {
    return _deliveryRepository.listenDeliveriesOwner(newState, myUser);
  }

  Future<void> init() async {
    emit(OrdersOwnerLoadingState());
    const GeoPoint geo = const GeoPoint(4.710493684827736, -74.04221545820411);
    final Delivery deliveryDesAndOr = Delivery('', geo);
    final DocumentReference docRefRestaurant =
        FirebaseFirestore.instance.collection('restaurant').doc('0');
    final DocumentReference docRefUser =
        FirebaseFirestore.instance.collection('user').doc('0');
    final double val = await providerLocal.getDeliveryPrice();
    double delPrice = 0;
    if (val != -1) {
      delPrice = val;
    }
    _myDelivery = MyDelivery('', '', deliveryDesAndOr, deliveryDesAndOr, '', '',
        docRefRestaurant, DateTime.now(), 0, docRefUser, '', false,
        deliveryPrice: delPrice);
    _deliveryGuys = await MyUserRepository().getDeliveryGuys();
    emit(OrdersOwnerReadyState(_myDelivery, _deliveryGuys));
  }

  Future<void> saveMyDelivery(
      String id,
      String clientName,
      Delivery deliveryDestination,
      Delivery deliveryOrigin,
      String orderID,
      String phone,
      DateTime startDate,
      int status,
      bool willPayOnDelivery,
      String restaurantID,
      String userID,
      String startDateFormated,
      double deliveryPrice) async {
    DocumentReference docRefRestaurant =
        FirebaseFirestore.instance.collection('restaurant').doc(restaurantID);
    DocumentReference docRefUser =
        FirebaseFirestore.instance.collection('user').doc(userID);
    _myDelivery = MyDelivery(
        id,
        clientName,
        deliveryDestination,
        deliveryOrigin,
        orderID,
        phone,
        docRefRestaurant,
        startDate,
        status,
        docRefUser,
        startDateFormated,
        willPayOnDelivery,
        deliveryPrice: deliveryPrice);
    emit(OrdersOwnerReadyState(_myDelivery, _deliveryGuys, isSaving: true));
    await _deliveryRepository.saveDelivery(_myDelivery);
    await providerLocal.updateDeliveryPrice(deliveryPrice);
    const GeoPoint geo = const GeoPoint(4.710493684827736, -74.04221545820411);
    final Delivery deliveryDesAndOr = Delivery('', geo);
    docRefRestaurant =
        FirebaseFirestore.instance.collection('restaurant').doc('0');
    docRefUser = FirebaseFirestore.instance.collection('user').doc('0');
    _myDelivery = MyDelivery('', '', deliveryDesAndOr, deliveryDesAndOr, '', '',
        docRefRestaurant, DateTime.now(), 0, docRefUser, '', false,
        deliveryPrice: deliveryPrice);
    emit(OrdersOwnerReadyState(_myDelivery, _deliveryGuys));
  }
}

abstract class OrdersOwnerState extends Equatable {
  @override
  List<Object> get props => [];
}

class OrdersOwnerLoadingState extends OrdersOwnerState {}

class OrdersOwnerInitialState extends OrdersOwnerState {}

class OrdersOwnerReadyState extends OrdersOwnerState {
  final MyDelivery delivery;
  final List<MyUser> deliveryGuys;

  final isSaving;

  OrdersOwnerReadyState(this.delivery, this.deliveryGuys,
      {this.isSaving = false});

  @override
  List<Object> get props => [delivery, deliveryGuys, isSaving];
}
