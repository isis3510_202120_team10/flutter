import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/provider/sharepreferences.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

const ROUTE_KEY = 'coordinates';

class MyDeliveryCubit extends Cubit<MyDeliveryState> {
  MyDeliveryRepositoryBase? _deliveryRepository;
  List<LatLng> polylineCoordinates = [];
  MyDeliveryCubit() : super(MyDeliveryInitialState());

  set deliveryRepository(MyDeliveryRepositoryBase repository) {
    _deliveryRepository = repository;
  }

  Future<void> loadCurrentDelivery() async {
    try {
      emit(MyDeliveryLoadingState());
      final delivery = await _deliveryRepository!.getMyCurrentDelivery();
      final _coordinatesListString = await SharePreferencesProvider()
          .get(ROUTE_KEY, defaultValue: '[]');
      polylineCoordinates =
          (json.decode(_coordinatesListString) as List).map((e) {
        final lat = e['lat'] as double;
        final lng = e['lng'] as double;
        return LatLng(lat, lng);
      }).toList();
      emit(MyDeliveryCompleteCurrentState(
          delivery: delivery, polylineCoordinates: polylineCoordinates));
      //listenCurrentDelivery((s)=>emit(MyDeliveryCompleteCurrentState(s)));
    } on Exception catch (e) {
      emit(MyDeliveryErrorState(e.toString()));
    }
  }

  Future<void> setCurrentDelivery( MyDelivery delivery) async {
    try {
      emit(MyDeliveryLoadingState());
      final _coordinatesListString = await SharePreferencesProvider()
          .get(ROUTE_KEY, defaultValue: '[]');
      polylineCoordinates =
          (json.decode(_coordinatesListString) as List).map((e) {
            final lat = e['lat'] as double;
            final lng = e['lng'] as double;
            return LatLng(lat, lng);
          }).toList();
      emit(MyDeliveryCompleteCurrentState(
          delivery: delivery, polylineCoordinates: polylineCoordinates));
      //listenCurrentDelivery((s)=>emit(MyDeliveryCompleteCurrentState(s)));
    } on Exception catch (e) {
      emit(MyDeliveryErrorState(e.toString()));
    }
  }
  Future<void> listenCurrentDelivery(Function fu) async {
    _deliveryRepository!.listenCurrentDelivery(fu);
  }

  Future<void> emitCurrent(MyDelivery? delivery) async {
    emit(MyDeliveryCompleteCurrentState(
        delivery: delivery, polylineCoordinates: polylineCoordinates));
  }

  Future<void> updateDelivery(MyDelivery delivery) async {
    try {
      await _deliveryRepository!.updateMyDelivery(delivery);
      emit(MyDeliveryCompleteCurrentState(
          delivery: delivery, polylineCoordinates: polylineCoordinates));
    } on Exception catch (e) {
      emit(MyDeliveryErrorState(e.toString()));
    }
  }

  Future<void> updateCoordinates(List<LatLng> pCoordinates) async {
    try {
      polylineCoordinates = pCoordinates;
      emit(MyDeliveryCompleteCurrentState(
          delivery: (state as MyDeliveryCompleteCurrentState).delivery,
          polylineCoordinates: polylineCoordinates));
      SharePreferencesProvider().set(ROUTE_KEY, json.encode(polylineCoordinates.map((e) => {'lat':e.latitude, 'lng':e.longitude}).toList()));
    } on Exception catch (e) {
      emit(MyDeliveryErrorState(e.toString()));
    }
  }

  Future<void> newDelivery(MyDelivery delivery, Function sa) async {
    emit(MyDeliveryLoadingState());
    sa(delivery);
    emit(MyDeliveryCompleteCurrentState(
        delivery: delivery, polylineCoordinates: polylineCoordinates));
  }

  void listenDelivery(MyDelivery delivery, Function newState) async {
    return _deliveryRepository!.listenDelivery(newState, delivery);
  }
}

abstract class MyDeliveryState extends Equatable {
  @override
  List<Object> get props => [];
}

class MyDeliveryLoadingState extends MyDeliveryState {}

class MyDeliveryInitialState extends MyDeliveryState {}

class MyDeliveryCompleteCurrentState extends MyDeliveryState {
  final MyDelivery? delivery;
  final List<LatLng> polylineCoordinates;

  MyDeliveryCompleteCurrentState(
      {required this.delivery, required this.polylineCoordinates});

  @override
  List<Object> get props =>
      delivery != null ? [delivery!, polylineCoordinates] : [];
}

class MyDeliveryErrorState extends MyDeliveryState {
  final String message;

  MyDeliveryErrorState(this.message);

  @override
  List<Object> get props => [message];
}
