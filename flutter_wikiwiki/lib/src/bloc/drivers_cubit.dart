import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';

class MyDriversCubit extends Cubit<MyDriversState> {
  final MyDeliveryRepositoryBase _deliveryRepository;
  MyDriversCubit(this._deliveryRepository) : super(MyDriversInitialState());
  Future<void> loadDeliveries() async {
    try {
      emit(MyDriversLoadingState());
      final deliveries =
      await _deliveryRepository.getCurrentDeliveries();
      emit(MyDriversCompleteState(deliveries));
    } on Exception catch (e) {
      emit(MyDriversErrorState(e.toString()));
    }
  }
}

abstract class MyDriversState extends Equatable {
  @override
  List<Object> get props => [];
}

class MyDriversLoadingState extends MyDriversState {}

class MyDriversInitialState extends MyDriversState {}

class MyDriversCompleteState extends MyDriversState {
  final List<MyDelivery> deliveries;

  MyDriversCompleteState(this.deliveries);

  @override
  List<Object> get props => [deliveries];
}

class MyDriversErrorState extends MyDriversState {
  final String message;

  MyDriversErrorState(this.message);

  @override
  List<Object> get props => [message];
}
