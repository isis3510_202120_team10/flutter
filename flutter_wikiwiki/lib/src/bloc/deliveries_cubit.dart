import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';

class MyDeliveriesCubit extends Cubit<MyDeliveriesState> {
  final MyDeliveryRepositoryBase _deliveryRepository;
  MyDeliveriesCubit(this._deliveryRepository) : super(MyDeliveriesInitialState());

  Future<void> loadDeliveriesFromMe(int status) async {
    try {
      emit(MyDeliveriesLoadingState());
      final deliveries =
      await _deliveryRepository.getMyDeliveriesByStatus(status);
      emit(MyDeliveriesCompleteState(deliveries));
    } on Exception catch (e) {
      emit(MyDeliveriesErrorState(e.toString()));
    }
  }
  Future<void> loadDeliveriesByStatus(int status) async {
    try {
      emit(MyDeliveriesLoadingState());
      final deliveries =
      await _deliveryRepository.getDeliveriesByStatus(status );
      emit(MyDeliveriesCompleteState(deliveries));
    } on Exception catch (e) {
      emit(MyDeliveriesErrorState(e.toString()));
    }
  }
}

abstract class MyDeliveriesState extends Equatable {
  @override
  List<Object> get props => [];
}

class MyDeliveriesLoadingState extends MyDeliveriesState {}

class MyDeliveriesInitialState extends MyDeliveriesState {}

class MyDeliveriesCompleteState extends MyDeliveriesState {
  final List<MyDelivery> deliveries;

  MyDeliveriesCompleteState(this.deliveries);

  @override
  List<Object> get props => [deliveries];
}

class MyDeliveriesErrorState extends MyDeliveriesState {
  final String message;

  MyDeliveriesErrorState(this.message);

  @override
  List<Object> get props => [message];
}
