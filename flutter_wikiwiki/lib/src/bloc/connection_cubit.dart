import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/provider/connection.dart';

class ConnectionCubit extends Cubit<MyConnectionState> {
  final ConnectionStatusSingleton _connection;
  late StreamSubscription _connectionChangeStream;

  ConnectionCubit(this._connection) : super(MyConnectionInitialState());
  Future<void> init() async {
    _connectionChangeStream =
        Connectivity().onConnectivityChanged.listen((state) {
      connectionChanged(state != ConnectivityResult.none);
    });
  }

  void connectionChanged(dynamic connection) {
    emit(MyConnectionCompleteState(connection));
  }

  void firstRun() async {
    final status = await _connection.checkConnection();
    emit(MyConnectionCompleteState(status));
  }
}

abstract class MyConnectionState extends Equatable {
  @override
  List<Object> get props => [];
}

class MyConnectionLoadingState extends MyConnectionState {}

class MyConnectionInitialState extends MyConnectionState {}

class MyConnectionCompleteState extends MyConnectionState {
  final bool connected;

  MyConnectionCompleteState(this.connected);

  @override
  List<Object> get props => [connected];
}

class ConnectionErrorState extends MyConnectionState {
  final String message;

  ConnectionErrorState(this.message);

  @override
  List<Object> get props => [message];
}
