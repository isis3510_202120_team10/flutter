import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/repository/routes_repository.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class RoutesCubit extends Cubit<RoutesState> {
  final RoutesRepositoryBase _routesRepository;

  late final List<LatLng> previousRoute;

  // void setRoutes(List) {}


  RoutesCubit(RoutesState initialState, this._routesRepository) : super(initialState);
}

abstract class RoutesState extends Equatable {
  @override
  List<Object?> get props => [];
}

class PreviousRoutes extends RoutesState{
  late final List<LatLng> previousRoute;

  PreviousRoutes(this.previousRoute);

  @override
  List<Object> get props => [previousRoute];

  
}