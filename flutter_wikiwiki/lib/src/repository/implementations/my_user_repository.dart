import 'dart:io';

import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/provider/firebase_provider.dart';
import 'package:flutter_wikiwiki/src/repository/my_user_repository.dart';

class MyUserRepository extends MyUserRepositoryBase {
  final provider = FirebaseProvider();

  @override
  Future<MyUser?> getMyUser() => provider.getMyUser();

  @override
  Future<List<MyUser>> getDeliveryGuys() => provider.getDeliveryGuys();

  @override
  Future<void> saveMyUser(MyUser user, File? image) =>
      provider.saveMyUser(user, image);
}
