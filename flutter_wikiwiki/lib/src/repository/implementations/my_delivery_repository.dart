import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/provider/firebase_provider.dart';

import '../my_delivery_repository.dart';

class MyDeliveryRepository extends MyDeliveryRepositoryBase {
  final provider = FirebaseProvider();

  @override
  Future<List<MyDelivery>> getMyDeliveries() {
    return provider.getMyDeliveries();
  }

  @override
  Future<void> updateMyDelivery(MyDelivery currentDelivery) {
    if (currentDelivery.status != 2) {
      currentDelivery.status++;
      if (currentDelivery.status == 2) {
        currentDelivery.finishDate = DateTime.now();
      }
    }
    return provider.updateMyDelivery(currentDelivery);
  }

  @override
  Future<List<MyDelivery>> getMyDeliveriesByStatus(int status) {
    return provider.getMyDeliveriesByStatus(status);
  }

  @override
  Future<MyDelivery?> getMyCurrentDelivery() {
    return provider.getMyCurrentDelivery();
  }

  @override
  void listenDelivery(Function fu, MyDelivery delivery) {
    return provider.listenDelivery(fu, delivery);
  }

  @override
  void listenCurrentDelivery(Function fu) {
    return provider.listenCurrentDelivery(fu);
  }

  @override
  Future<List<MyDelivery>> getDeliveriesByStatus(int status) {
    return provider.getDeliveriesByStatus(status);
  }
  @override
  void listenDeliveriesOwner(Function fu, MyUser myUser) {
    return provider.listenDeliveriesOwner(fu, myUser);
  }

  @override
  Future<void> saveDelivery(MyDelivery myDelivery) =>
      provider.saveDelivery(myDelivery);

  @override
  Future<List<MyDelivery>> getCurrentDeliveries() {
    return provider.getCurrentDeliveries();
  }
}
