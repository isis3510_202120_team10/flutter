import 'package:flutter_wikiwiki/src/provider/rest_service.dart';
import 'package:flutter_wikiwiki/src/provider/sharepreferences.dart';

import '../wait_time_repository.dart';

class WaitTimeRepository extends WaitTimeRepositoryBase {
  final providerLocal = SharePreferencesProvider();
  final providerHttps = CloudFunctionsRestService();

  @override
  Future<String> getWaitTimeLocal(String userId) {
    return Future<String>.value(providerLocal.getWaitTime(userId));
  }

  @override
  Future<String> getWaitTimeRemote(String userId) {
    return providerHttps.getAverageTimeDelivery(userId);
  }

  @override
  Future<void> init() {
    return providerLocal.init();
  }

  @override
  Future<void> saveWaitTimeLocal(String time, String userId) {
    return providerLocal.updateWaitTime(userId, time);
  }



}