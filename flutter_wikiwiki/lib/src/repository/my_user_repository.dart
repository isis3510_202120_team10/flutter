import 'dart:io';

import 'package:flutter_wikiwiki/src/model/user.dart';

abstract class MyUserRepositoryBase {
  Future<MyUser?> getMyUser();

  Future<List<MyUser>> getDeliveryGuys();

  Future<void> saveMyUser(MyUser user, File? image);
}
