import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class RoutesRepositoryBase {
  Future<List<LatLng>> getPolylineCoordinates();
}