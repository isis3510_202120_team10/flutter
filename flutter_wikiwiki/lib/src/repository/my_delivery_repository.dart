import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';

abstract class MyDeliveryRepositoryBase {
  Future<List<MyDelivery>> getMyDeliveries();

  Future<List<MyDelivery>> getMyDeliveriesByStatus(int status);

  Future<List<MyDelivery>> getDeliveriesByStatus(int status);

  Future<List<MyDelivery>> getCurrentDeliveries();

  Future<void> updateMyDelivery(MyDelivery currentDelivery);

  void listenDelivery(Function fu, MyDelivery delivery);

  void listenCurrentDelivery(Function fu);

  void listenDeliveriesOwner(Function fu, MyUser myUser);

  Future<void> saveDelivery(MyDelivery myDelivery);

  Future<MyDelivery?> getMyCurrentDelivery();
}
