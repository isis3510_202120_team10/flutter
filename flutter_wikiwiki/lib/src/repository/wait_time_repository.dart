import 'dart:io';

abstract class WaitTimeRepositoryBase {
  Future<void> init();

  Future<String> getWaitTimeLocal(String userId);

  Future<String> getWaitTimeRemote(String userId);

  Future<void> saveWaitTimeLocal(String time, String userId );
}
