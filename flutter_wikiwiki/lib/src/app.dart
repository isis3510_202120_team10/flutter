import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/configuration.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/navigation/routes.dart';
import 'package:flutter_wikiwiki/src/provider/services.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_user_repository.dart';

final _navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  const MyApp({
    Key? key,
    required AnalyticsService analyticsService,
  })  : _analyticsService = analyticsService,
        super(key: key);

  final AnalyticsService _analyticsService;

  static Widget create({
    required AnalyticsService analyticsService,
  }) {
    return BlocListener<AuthCubit, AuthState>(
        listener: (context, state) async {
          if (state is AuthSignedOut) {
            _navigatorKey.currentState
                ?.pushNamedAndRemoveUntil(Routes.intro, (route) => false);
          } else if (state is AuthSignedIn) {
            final MyUser? user = await MyUserRepository().getMyUser();
            if (user != null && user.role == 'OWNER') {
              _navigatorKey.currentState?.pushNamedAndRemoveUntil(
                  Routes.createOrderOwner, (route) => false);
            } else {
              _navigatorKey.currentState
                  ?.pushNamedAndRemoveUntil(Routes.home, (route) => false);
            }
          }
        },
        child: MyApp(analyticsService: analyticsService));
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (BuildContext context) => kAppName,
      navigatorKey: _navigatorKey,
      title: 'WikiWiki',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.blue,
        ).copyWith(
          primary: const Color(0xff19B366),
          secondary: const Color(0xff5EE8A3),
        ),
        textTheme: const TextTheme(
            headline6: TextStyle(color: Color(0xff19B366)),
            bodyText1: TextStyle(color: Color(0xffffffff)),
            bodyText2: TextStyle(color: Color(0xff1A1A1A)),
            button: TextStyle(color: Color(0xffFFFFFF))),
      ),
      navigatorObservers: kIsWeb == false && kUseFirebaseAnalytics
          ? <NavigatorObserver>[widget._analyticsService.getAnalyticsObserver()]
          : <NavigatorObserver>[],
      onGenerateRoute: Routes.routes,
    );
  }
}
