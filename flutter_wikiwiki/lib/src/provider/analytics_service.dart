import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';

Map<String, Object> filterOutNulls(Map<String, Object?>? parameters) {
  final Map<String, Object> filtered = <String, Object>{};
  if (parameters != null) {
    parameters.forEach((String key, Object? value) {
      if (value != null) {
        filtered[key] = value;
      }
    });
  }
  return filtered;
}

class AnalyticsService {
  AnalyticsService({this.useGoogleAnalytics = true});

  final bool useGoogleAnalytics;
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future<void> setUserProperties({@required String? userId}) async {
    if (useGoogleAnalytics) {
      await _analytics.setUserId(userId);
    }
  }

  Future<void> logSignInOrUp(name, Map<String, dynamic>? parameters) async {
    if (useGoogleAnalytics) {
      await _analytics.logEvent(
          name: name, parameters: filterOutNulls(parameters));
    }
  }

  Future<void> logSignOut([Map<String, dynamic>? parameters]) async {
    if (useGoogleAnalytics) {
      await _analytics.logEvent(
          name: 'sign_out', parameters: filterOutNulls(parameters));
    }
  }
}
