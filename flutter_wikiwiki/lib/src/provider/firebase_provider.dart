import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';

import 'package:path/path.dart' as path;

class FirebaseProvider {
  FirebaseStorage get storage => FirebaseStorage.instance;

  FirebaseFirestore get firestore => FirebaseFirestore.instance;

  User get currentUser {
    final user = FirebaseAuth.instance.currentUser;

    if (user == null) {
      throw Exception('Not authenticated exception');
    }

    return user;
  }

  Future<List<MyDelivery>> getMyDeliveries() async {
    final userRef =
        FirebaseFirestore.instance.collection('user').doc(currentUser.uid);
    final deliveries = await FirebaseFirestore.instance
        .collection('delivery')
        .where('user', isEqualTo: userRef)
        .get();
    final deliveriesList = deliveries.docs
        .map((delivery) =>
            MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
        .toList();
    return deliveriesList;
  }

  Future<List<MyDelivery>> getMyDeliveriesByStatus(int status) async {
    final userRef =
        FirebaseFirestore.instance.collection('user').doc(currentUser.uid);
    final deliveries = await FirebaseFirestore.instance
        .collection('delivery')
        .where('user', isEqualTo: userRef)
        .where('status', isEqualTo: status)
        .get();
    final deliveriesList = deliveries.docs
        .map((delivery) =>
            MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
        .toList();
    for (var i = 0; i < deliveriesList.length; i++) {
      await deliveriesList[i].setRestaurant();
    }
    return deliveriesList;
  }

  Future<List<MyDelivery>> getCurrentDeliveries() async {
    final snapshot = await firestore.doc('user/${currentUser.uid}').get();
    final deliveries = await FirebaseFirestore.instance
        .collection('delivery')
        .where('restaurant', isEqualTo: snapshot.data()!['restaurant'])
        .get();
    final deliveriesList = deliveries.docs
        .map((delivery) =>
            MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
        .toList();
    deliveriesList.sort((a, b) => a.startDate.compareTo(b.startDate));
    final List<MyDelivery> res = [];
    final Map<String, bool> map = {};
    for (var i = 0; i < deliveriesList.length; i++) {
      if (!map.containsKey(deliveriesList[i].userID.id)) {
        map[deliveriesList[i].userID.id] = true;
        await deliveriesList[i].setUser();
        res.add(deliveriesList[i]);
      }
    }
    return res;
  }

  Future<List<MyDelivery>> getDeliveriesByStatus(int status) async {
    final snapshot = await firestore.doc('user/${currentUser.uid}').get();
    final deliveries = await FirebaseFirestore.instance
        .collection('delivery')
        .where('restaurant', isEqualTo: snapshot.data()!['restaurant'])
        .where('status', isEqualTo: status)
        .get();
    final deliveriesList = deliveries.docs
        .map((delivery) =>
            MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
        .toList();
    deliveriesList.sort((a, b) => a.startDate.compareTo(b.startDate));
    for (var i = 0; i < deliveriesList.length; i++) {
      await deliveriesList[i].setRestaurant();
    }
    return deliveriesList;
  }

  Future<MyDelivery?> getMyCurrentDelivery() async {
    final userRef =
        FirebaseFirestore.instance.collection('user').doc(currentUser.uid);
    final deliveries = await FirebaseFirestore.instance
        .collection('delivery')
        .where('user', isEqualTo: userRef)
        .where('status', isLessThanOrEqualTo: 1)
        .get();
    final deliveriesList = deliveries.docs
        .map((delivery) =>
            MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
        .toList();
    deliveriesList.sort((a, b) => a.startDate.compareTo(b.startDate));
    return deliveriesList.isNotEmpty ? deliveriesList[0] : null;
  }

  Future<void> updateMyDelivery(MyDelivery delivery) async {
    return FirebaseFirestore.instance
        .collection('delivery')
        .doc(delivery.id)
        .update(delivery.toFirebaseMap());
  }

  void listenDelivery(Function fu, MyDelivery currentDelivery) async {
    FirebaseFirestore.instance
        .collection('delivery')
        .doc(currentDelivery.id)
        .snapshots()
        .listen((doc) {
      final data = doc.data()!;
      final MyDelivery deliveryData = MyDelivery.fromFirebaseMap(data, doc.id);
      fu(deliveryData);
    });
  }

  void listenDeliveriesOwner(Function function, MyUser myUser) {
    FirebaseFirestore.instance
        .collection('delivery')
        .orderBy('age', descending: true)
        .limit(5)
        .where('restaurant', isEqualTo: myUser.restaurant)
        .snapshots()
        .listen((doc) {
      final data = doc.docs
          .map((delivery) =>
              MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
          .toList();
      function(data);
    });
  }

  Future<void> saveDelivery(MyDelivery myDelivery) async {
    final CollectionReference deliveries =
        FirebaseFirestore.instance.collection('delivery');

    await deliveries.add(myDelivery.newToFirebaseMap());
  }

  void listenCurrentDelivery(Function fu) async {
    final userRef =
        FirebaseFirestore.instance.collection('user').doc(currentUser.uid);
    FirebaseFirestore.instance
        .collection('delivery')
        .where('user', isEqualTo: userRef)
        .where('status', isLessThanOrEqualTo: 1)
        .snapshots()
        .listen((snapshot) {
      final deliveriesList = snapshot.docs
          .map((delivery) =>
              MyDelivery.fromFirebaseMap(delivery.data(), delivery.id))
          .toList();
      deliveriesList.sort((a, b) => -1 * a.startDate.compareTo(b.startDate));
      final deliveryData = deliveriesList.isNotEmpty ? deliveriesList[0] : null;
      fu(deliveryData);
    });
  }

  Future<MyUser?> getMyUser() async {
    final snapshot = await firestore.doc('user/${currentUser.uid}').get();
    if (snapshot.exists) {
      return MyUser.fromFirebaseMap(snapshot.data()!);
    }
    return null;
  }

  Future<List<MyUser>> getDeliveryGuys() async {
    final deliveries = await FirebaseFirestore.instance
        .collection('user')
        .where('role', isEqualTo: 'DRIVER')
        .orderBy('name')
        .get();
    final usersList = deliveries.docs
        .map((user) => MyUser.fromFirebaseMap(user.data()))
        .toList();
    return usersList;
  }

  Future<void> saveMyUser(MyUser user, File? image) async {
    final ref = firestore.doc('user/${currentUser.uid}');

    if (image == null) {
      await ref.set(user.toFirebaseMap(), SetOptions(merge: true));
    } else {
      final imagePath =
          '${currentUser.uid}/profile/${path.basename(image.path)}';

      final storageRef = storage.ref(imagePath);
      await storageRef.putFile(image);
      final url = await storageRef.getDownloadURL();
      await ref.set(user.toFirebaseMap(newImage: url), SetOptions(merge: true));
    }
  }
}
