import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharePreferencesProvider {
  late SharedPreferences prefs;

  static const String pinAttempts = 'pin_attempts';
  static const String pinLockUntil = 'lock_duration';
  static const String deliveryPrice = 'delivery_price';

  Future<dynamic> get(String key, {dynamic defaultValue}) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    return sharedPreferences.get(key) ?? defaultValue;
  }

  Future<void> set(String key, value) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    if (value is bool) {
      sharedPreferences.setBool(key, value);
    } else if (value is String) {
      sharedPreferences.setString(key, value);
    } else if (value is double) {
      sharedPreferences.setDouble(key, value);
    } else if (value is int) {
      sharedPreferences.setInt(key, value);
    }
  }

  Future<void> init() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<void> updateWaitTime(String userId, String time) async {
    await set(userId, time);
  }

  String getWaitTime(String userId) {
    final res = prefs.getString(userId);
    return res ?? 'failed';
  }

  Future<void> updateDeliveryPrice(double newPrice) async {
    await set(deliveryPrice, newPrice);
  }

  Future<double> getDeliveryPrice() async {
    return await get(deliveryPrice, defaultValue: -1.0);
  }

  Future<void> updatePinAttemps(int attemps) async {
    prefs.setInt(pinAttempts, attemps);
  }

  Future<int> getLockAttempts() async {
    return await get(pinAttempts, defaultValue: 0);
  }

  Future<void> incrementLockAttempts() async {
    await set(pinAttempts, await getLockAttempts() + 1);
  }

  Future<void> resetPinLockUntil() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(pinLockUntil);
  }

  Future<void> resetLockAttempts() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(pinAttempts);
    await prefs.remove(pinLockUntil);
  }

  Future<bool> shouldLock() async {
    if (await get(pinLockUntil) != null) {
      return true;
    }
    return false;
  }

  Future<void> updateLockDate() async {
    final int attempts = await getLockAttempts();
    if (attempts >= 20) {
      // 4+ failed attempts
      await set(
          pinLockUntil,
          DateFormat.yMd()
              .add_jms()
              .format(DateTime.now().toUtc().add(const Duration(minutes: 5))));
    } else if (attempts >= 15) {
      // 3 failed attempts
      await set(
          pinLockUntil,
          DateFormat.yMd()
              .add_jms()
              .format(DateTime.now().toUtc().add(const Duration(minutes: 2))));
    } else if (attempts >= 10) {
      // 2 failed attempts
      await set(
          pinLockUntil,
          DateFormat.yMd()
              .add_jms()
              .format(DateTime.now().toUtc().add(const Duration(seconds: 30))));
    } else if (attempts >= 5) {
      await set(
          pinLockUntil,
          DateFormat.yMd()
              .add_jms()
              .format(DateTime.now().toUtc().add(const Duration(seconds: 10))));
    }
  }

  Future<DateTime?> getLockDate() async {
    final String lockDateStr = await get(pinLockUntil) ?? '';
    if (lockDateStr == '') {
      return null;
    }
    return DateFormat.yMd().add_jms().parseUtc(lockDateStr);
  }
}
