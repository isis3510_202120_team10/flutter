import 'package:http/http.dart' as http;

class CloudFunctionsRestService {
  CloudFunctionsRestService();
  static const _endpoint = 'us-central1-wikiwiki-moviles2021-20.cloudfunctions.net';

  Future<String> getAverageTimeDelivery(String userId) async {
      final params = {'user': userId};
      final url = Uri.https(_endpoint, '/averageTimeDelivery/', params);
      final http.Response response =  await http.get(url);
      try {
        if (response.statusCode != 200) {
          return 'failed';
        }
        final String data = response.body;
        if (data == '0')
        {
          return 'failed';
        }
        return data;

      } catch (e) {
        return 'failed';
      }
  }
}
