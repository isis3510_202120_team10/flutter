import 'package:flutter/material.dart';

class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget image = Image.asset(
      'assets/wikiwiki.jpg',
      fit: BoxFit.fill,
    );

    return ClipOval(
      child: SizedBox(
        width: 150,
        height: 150,
        child: image,
      ),
    );
  }
}
