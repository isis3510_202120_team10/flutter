import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';

import 'info.dart';

class Body extends StatelessWidget {
  const Body(
      {Key? key,
      required this.image,
      required this.user,
      required this.authUserUid})
      : super(key: key);
  final Widget image;
  final MyUser? user;
  final String? authUserUid;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Info(
              image: image,
              name: user?.name ?? '',
              lastName: user?.lastName ?? ''),
          Container(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text('Phone',
                        style: TextStyle(
                          color: Theme.of(context).textTheme.headline6!.color,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  const Divider(
                    height: 20,
                    thickness: 1,
                    indent: 0,
                    endIndent: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(user?.phoneNumber ?? '',
                        style: const TextStyle(
                          fontSize: 16,
                        )),
                  ),
                  const SizedBox(height: 30),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text('Role',
                        style: TextStyle(
                          color: Theme.of(context).textTheme.headline6!.color,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  const Divider(
                    height: 20,
                    thickness: 1,
                    indent: 0,
                    endIndent: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(user?.role ?? '',
                        style: const TextStyle(
                          fontSize: 16,
                        )),
                  ),
                  const SizedBox(height: 30),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text('UID',
                        style: TextStyle(
                          color: Theme.of(context).textTheme.headline6!.color,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  const Divider(
                    height: 20,
                    thickness: 1,
                    indent: 0,
                    endIndent: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(authUserUid ?? '',
                        style: const TextStyle(
                          fontSize: 16,
                        )),
                  ),
                  const SizedBox(height: 12),
                  BlocBuilder<AuthCubit, AuthState>(
                      buildWhen: (previous, current) => current is AuthSignedIn,
                      builder: (context, state) {
                        final authUser = (state as AuthSignedIn).user;
                        return Center(
                            child: ElevatedButton(
                                onPressed: () =>
                                    context.read<AuthCubit>().signOut(authUser),
                                child: const Text('Sign out')));
                      }),
                ],
              )),
        ],
      ),
    );
  }
}
