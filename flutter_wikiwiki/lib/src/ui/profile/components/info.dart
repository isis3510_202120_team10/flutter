import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/blocs.dart';

// ignore: must_be_immutable
class Info extends StatelessWidget {
  Info({
    Key? key,
    this.name = '',
    this.lastName = '',
    required this.image,
  }) : super(key: key);
  final String name, lastName;
  Widget image;

  @override
  Widget build(BuildContext context) {
    const double defaultSize = 10;
    return SizedBox(
      height: defaultSize * 24,
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: CustomShape(),
            child: Container(
              height: defaultSize * 15,
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: defaultSize),
                  height: defaultSize * 14,
                  width: defaultSize * 14,
                  child: ClipOval(
                    child: SizedBox(
                      width: 150,
                      height: 150,
                      child: image,
                    ),
                  ),
                ),
                Text(
                  name + ' ' + lastName,
                  style: TextStyle(
                    fontSize: defaultSize * 2.2,
                  ),
                ),
                SizedBox(height: defaultSize / 2),
                BlocBuilder<AuthCubit, AuthState>(
                  buildWhen: (_, current) => current is AuthSignedIn,
                  builder: (_, state) {
                    return Center(
                      child: Text(
                        '${(state as AuthSignedIn).user.email}',
                        style: const TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF8492A2),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    final double height = size.height;
    final double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
