import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/blocs.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/my_user_cubit.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/ui/profile/components/body.dart';
import 'package:flutter_wikiwiki/src/ui/profile/profile_screen_edit.dart';
import 'package:flutter_wikiwiki/src/ui/utils/no_connection_toastr.dart';

// ignore: must_be_immutable
class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key, this.screen = 0}) : super(key: key);
  int screen = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: BlocBuilder<MyUserCubit, MyUserState>(
      builder: (cont, state) {
        if (state is MyUserReadyState) {
          if (screen == 0) {
            return _MyUserSection(
              user: state.user,
              pickedImage: state.pickedImage,
            );
          }
          return ProfileEditScreen(
            user: state.user,
            pickedImage: state.pickedImage,
            isSaving: state.isSaving,
          );
        }
        return const Center(child: CircularProgressIndicator());
      },
    ));
  }
}

class _MyUserSection extends StatefulWidget {
  final MyUser? user;
  final File? pickedImage;

  const _MyUserSection({this.user, this.pickedImage});

  @override
  __MyUserSectionState createState() => __MyUserSectionState();
}

class __MyUserSectionState extends State<_MyUserSection> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectionCubit, MyConnectionState>(
        builder: (context, stateConnection) {
      if (stateConnection is MyConnectionCompleteState &&
          !stateConnection.connected) {
        toastMessage(context);
      }
      return BlocBuilder<AuthCubit, AuthState>(
        buildWhen: (previous, current) => current is AuthSignedIn,
        builder: (context, state) {
          final authUserUid = (state as AuthSignedIn).user.uid;
          Widget image = Image.asset(
            'assets/wikiwiki.jpg',
            fit: BoxFit.fill,
          );

          if (widget.pickedImage != null) {
            image = Image.file(
              widget.pickedImage!,
              fit: BoxFit.fill,
            );
          } else if (widget.user?.image != null &&
              widget.user!.image!.isNotEmpty) {
            image = CachedNetworkImage(
              imageUrl: widget.user!.image!,
              progressIndicatorBuilder: (_, __, progress) =>
                  CircularProgressIndicator(
                      value: progress.progress, color: Colors.indigoAccent),
              errorWidget: (_, __, ___) => const Icon(Icons.error),
              fit: BoxFit.fill,
            );
          }

          return Body(
            image: image,
            user: widget.user,
            authUserUid: authUserUid,
          );
        },
      );
    });
  }
}
