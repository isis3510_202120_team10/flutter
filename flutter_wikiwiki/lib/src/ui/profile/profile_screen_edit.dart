import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/blocs.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/my_user_cubit.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_user_repository.dart';
import 'package:flutter_wikiwiki/src/ui/navegation_bar.dart';
import 'package:flutter_wikiwiki/src/ui/navegation_bar_owner.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class ProfileEditScreen extends StatefulWidget {
  final MyUser? user;
  final File? pickedImage;
  final bool isSaving;

  const ProfileEditScreen(
      {Key? key, this.user, this.pickedImage, this.isSaving = false})
      : super(key: key);

  @override
  _ProfileEditScreenState createState() => _ProfileEditScreenState();
}

class _ProfileEditScreenState extends State<ProfileEditScreen> {
  final _nameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _phoneNumberController = TextEditingController();

  final _picker = ImagePicker();

  @override
  void initState() {
    _nameController.text = widget.user?.name ?? '';
    _lastNameController.text = widget.user?.lastName ?? '';
    _phoneNumberController.text = widget.user?.phoneNumber ?? '';
    super.initState();
  }

  bool showBanner = true;

  void _toggleBanner() {
    setState(() {
      showBanner = !showBanner;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      buildWhen: (previous, current) => current is AuthSignedIn,
      builder: (context, state) {
        Widget image = Image.asset(
          'assets/wikiwiki.jpg',
          fit: BoxFit.fill,
        );

        if (widget.pickedImage != null) {
          image = Image.file(
            widget.pickedImage!,
            fit: BoxFit.fill,
          );
        } else if (widget.user?.image != null &&
            widget.user!.image!.isNotEmpty) {
          image = CachedNetworkImage(
            imageUrl: widget.user!.image!,
            progressIndicatorBuilder: (_, __, progress) =>
                CircularProgressIndicator(value: progress.progress),
            errorWidget: (_, __, ___) => const Icon(Icons.error),
            fit: BoxFit.fill,
          );
        }

        return SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () async {
                    final pickedImage =
                        await _picker.pickImage(source: ImageSource.camera);
                    if (pickedImage != null) {
                      context
                          .read<MyUserCubit>()
                          .setImage(File(pickedImage.path));
                    }
                  },
                  child: Center(
                    child: ClipOval(
                      child: SizedBox(
                        width: 150,
                        height: 150,
                        child: image,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 8),
                BlocBuilder<AuthCubit, AuthState>(
                  buildWhen: (_, current) => current is AuthSignedIn,
                  builder: (_, state) {
                    return Center(
                      child: Text('UID: ${(state as AuthSignedIn).user.uid}'),
                    );
                  },
                ),
                TextField(
                  controller: _nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                const SizedBox(height: 8),
                TextField(
                  controller: _lastNameController,
                  decoration: const InputDecoration(labelText: 'Last Name'),
                ),
                const SizedBox(height: 8),
                TextField(
                  controller: _phoneNumberController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(labelText: 'Phone number'),
                ),
                const SizedBox(height: 8),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    ElevatedButton(
                      child: const Text('Save'),
                      onPressed: widget.isSaving
                          ? null
                          : () async {
                              await context.read<MyUserCubit>().saveMyUser(
                                    (context.read<AuthCubit>().state
                                            as AuthSignedIn)
                                        .user
                                        .uid,
                                    _nameController.text,
                                    _lastNameController.text,
                                    _phoneNumberController.text,
                                    widget.user!.role,
                                  );
                              Fluttertoast.showToast(
                                  msg: 'Information updated correctly',
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  backgroundColor: Colors.green,
                                  textColor: Colors.white,
                                  fontSize: 12.0);
                              final MyUser? user =
                                  await MyUserRepository().getMyUser();
                              if (user != null && user.role == 'OWNER') {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          NavigationBarOwner.create(
                                              context, 3)),
                                );
                              } else {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          NavigationBar.create(context, 2)),
                                );
                              }
                            },
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                BlocBuilder<ConnectionCubit, MyConnectionState>(
                  builder: (context, connectionState) {
                    if (widget.isSaving &&
                        connectionState is MyConnectionCompleteState &&
                        !connectionState.connected &&
                        showBanner) {
                      return MaterialBanner(
                          padding: const EdgeInsets.all(15),
                          content: const Text(
                            'You do not have an internet connection at this time. Your data has been temporarily saved until you reconnect. Please do not close the application forcibly to avoid data loss.',
                          ),
                          leading: const Icon(Icons.warning),
                          backgroundColor: Colors.amber,
                          actions: <Widget>[
                            TextButton(
                              child: const Text('DISMISS'),
                              onPressed: () => _toggleBanner(),
                            ),
                          ]);
                    }
                    return Container();
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
