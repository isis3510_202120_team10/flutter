import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';

import 'order_detail_screen.dart';

class JoinOrderScreen extends StatefulWidget {
  const JoinOrderScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) {
    return const JoinOrderScreen();
  }

  @override
  _JoinOrderScreenState createState() => _JoinOrderScreenState();
}

class _JoinOrderScreenState extends State<JoinOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Order detail'), centerTitle: true),
        body: BlocBuilder<ConnectionCubit, MyConnectionState>(
            builder: (context, stateConnection) {
          return OrderDetailScreen(stateConnection);
        }));
  }
}
