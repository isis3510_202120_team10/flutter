import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/my_user_cubit.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_user_repository.dart';
import 'package:flutter_wikiwiki/src/ui/create_order/create_order_screen.dart';
import 'package:flutter_wikiwiki/src/ui/previous_delivery_screen.dart';
import 'package:flutter_wikiwiki/src/ui/profile/profile_screen.dart';
import 'delivery_man_screen.dart';

/// This is the stateful widget that the main application instantiates.
// ignore: must_be_immutable
class NavigationBarOwner extends StatefulWidget {
  int page;
  int selectedProfile = 0;

  NavigationBarOwner({Key? key, required this.page}) : super(key: key);

  static Widget create(BuildContext context, int newPage) {
    return BlocProvider(
      create: (_) => MyUserCubit(MyUserRepository())..getMyUser(),
      child: NavigationBarOwner(page: newPage),
    );
  }

  @override
  State<NavigationBarOwner> createState() => _NavigationBarOwnerState(
      selectedIndex: page, selectedProfile: selectedProfile);
}

/// This is the private State class that goes with MyStatefulWidget.
class _NavigationBarOwnerState extends State<NavigationBarOwner> {
  int selectedIndex;
  int selectedProfile;
  _NavigationBarOwnerState(
      {required this.selectedIndex, required this.selectedProfile})
      : super();

  Future _onItemTapped(int index) async {
    setState(() {
      selectedIndex = index;
    });
  }

  Future _onEditProfileTapped(int index) async {
    setState(() {
      selectedProfile = index;
    });
  }

  Widget _getPage(BuildContext context) {
    switch (selectedIndex) {
      case 0:
        setState(() {
          selectedProfile = 0;
        });
        return DeliveryManScreen.create(context);
      case 1:
        setState(() {
          selectedProfile = 0;
        });
        return PreviousScreen.create(context, true);
      case 2:
        setState(() {
          selectedProfile = 0;
        });
        return CreateOrderScreen.create(context);
      case 3:
        return ProfileScreen(screen: selectedProfile);
      default:
        return const Scaffold(body: Text('Not Found'));
    }
  }

  @override
  Widget build(BuildContext context) {
    context.read<MyUserCubit>().getMyUser();
    const List<String> titles = [
      'Drivers',
      'Orders',
      'Create order',
      'Profile',
      'Profile edit',
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(titles[selectedIndex]),
        centerTitle: true,
        actions: selectedIndex == 3
            ? <Widget>[
                TextButton(
                  onPressed: () {
                    _onEditProfileTapped(selectedProfile == 0 ? 1 : 0);
                  },
                  child: const Text(
                    'Edit',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ]
            : <Widget>[],
        elevation: 0,
      ),
      body: _getPage(context),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag),
            label: 'Drivers',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            label: 'Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_box),
            label: 'Create order',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
      ),
    );
  }
}
