import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/delivery_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/drivers_cubit.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/navigation/routes.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';
import 'package:flutter_wikiwiki/src/ui/utils/no_connection_toastr.dart';
import 'package:flutter_wikiwiki/src/ui/utils/search_widget.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DeliveryManScreen extends StatefulWidget {
  const DeliveryManScreen({Key? key}) : super(key: key);
  @override
  _DeliveryManScreenState createState() => _DeliveryManScreenState();

  static Widget create(BuildContext context) {
    return BlocProvider<MyDriversCubit>(
      create: (_) {
        final repository = context.read<MyDeliveryRepositoryBase>();
        return MyDriversCubit(repository)..loadDeliveries();
      },
      child: const DeliveryManScreen(),
    );
  }
}

class _DeliveryManScreenState extends State<DeliveryManScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectionCubit, MyConnectionState>(
        builder: (context, stateConnection) {
          if (stateConnection is MyConnectionCompleteState &&
              !stateConnection.connected) {
            toastMessage(context);
          }
          return BlocBuilder<AuthCubit, AuthState>(
              buildWhen: (previous, current) => current is AuthSignedIn,
              builder: (context, state) {
                return BlocBuilder<MyDriversCubit, MyDriversState>(
                    builder: (context, state) {
                      if (state is MyDriversLoadingState) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is MyDriversErrorState) {
                        return Text(state.message);
                      } else if (state is MyDriversCompleteState) {
                        final deliveries = state.deliveries;
                        return PreviousCompleteScreen(deliveries: deliveries);
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    });
              });
        });
  }
}

class PreviousCompleteScreen extends StatefulWidget {
  final List<MyDelivery> deliveries;
  const PreviousCompleteScreen({required this.deliveries});

  @override
  _PreviousCompleteScreenState createState() =>
      _PreviousCompleteScreenState(deliveries: deliveries);
}

class _PreviousCompleteScreenState extends State<PreviousCompleteScreen> {
  List<MyDelivery> deliveries;
  late List<MyDelivery> filterDeliveries;
  String query = '';
  _PreviousCompleteScreenState({required this.deliveries});

  @override
  void initState() {
    super.initState();
    filterDeliveries = deliveries;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: buildSearch(),
        elevation: 0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: ListView.builder(
          itemCount: filterDeliveries.length,
          itemBuilder: (_, int index) {
            return _ListItem(delivery: filterDeliveries[index]);
          }),
    );
  }

  Widget buildSearch() => SearchWidget(
    text: query,
    hintText: 'Search',
    onChanged: searchDelivery,
  );

  void searchDelivery(String query) {
    final searchLower = query.toLowerCase();
    final filterDeliveries = deliveries.where((delivery) {
      final name = delivery.getStatusText().toLowerCase();
      final userName = delivery.user!.getFullName().toLowerCase();
      final timeSpend = (delivery.finishDate ?? DateTime.now())
          .difference(delivery.startDate)
          .inMinutes
          .toString();
      return userName.contains(searchLower) ||
          timeSpend.contains(searchLower) ||
          name.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.filterDeliveries = filterDeliveries;
    });
  }
}

class _ListItem extends StatelessWidget {
  final MyDelivery delivery;

  const _ListItem({
    Key? key,
    required this.delivery,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final timeSpend = (delivery.finishDate ?? DateTime.now())
        .difference(delivery.startDate)
        .inMinutes
        .toString();
    final color = Theme.of(context).colorScheme.primary;
    final styleTitle = TextStyle(color: color, fontWeight: FontWeight.bold);
    if (delivery.user == null) return const Card();
    Widget image = Image.asset(
      'assets/wikiwiki.jpg',
      width: 75,
      height: 75,
      fit: BoxFit.fill,
    );

    if (delivery.user!.image != null && delivery.user!.image!.isNotEmpty) {
      image = CachedNetworkImage(
        width: 75,
        height: 75,
        placeholder: (context, url) => const CircularProgressIndicator(),
        imageUrl: delivery.user!.image!,
        errorWidget: (_, __, ___) => const Icon(Icons.error),
        fit: BoxFit.cover,
      );
    }
    return Center(
        child: Card(
            elevation: 5,
            margin: const EdgeInsets.all(15),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child:
            Wrap(direction: Axis.vertical, spacing: 15, children: <Widget>[
              Row(children: <Widget>[
                Padding(
                    padding: const EdgeInsets.fromLTRB(15, 15.0, 15.0, 0),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(150),
                        child: image)),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10.0, 10.0, 0),
                  child: Wrap(
                      direction: Axis.vertical,
                      spacing: 15,
                      children: <Widget>[
                        Row(children: <Widget>[
                          Icon(Icons.person, color: color, size: 15),
                          Text(" Driver: ", style: styleTitle),
                          Text(delivery.user!.getFullName())
                        ]),
                        Row(children: <Widget>[
                          Icon(Icons.calendar_today, color: color, size: 15),
                          Text(' Order status: ', style: styleTitle),
                          Text(delivery.getStatusText())
                        ]),
                        Row(children: <Widget>[
                          Icon(Icons.access_time, color: color, size: 15),
                          Text(' Est. delivery: ', style: styleTitle),
                          Text(timeSpend + ' min')
                        ]),
                      ]),
                )
              ]),
              Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 0, 10, 10.0),
                  child: Wrap(
                      direction: Axis.horizontal,
                      spacing: 15,
                      children: <Widget>[
                        Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.circular(
                                15,
                              ),
                            ),
                            padding: const EdgeInsets.all(8),
                            child: const Icon(
                              FontAwesomeIcons.whatsapp,
                              size: 20,
                            )),
                        SizedBox(
                            width: 185,
                            height: 40,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18.0),
                                      ))),
                              child: const Text('View order'),
                              onPressed: () {
                                context.read<MyDeliveryCubit>().setCurrentDelivery(delivery);
                                Navigator.of(context)
                                    .pushNamed(Routes.orderDetailOwner);
                              },
                            )),
                        Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.circular(
                                15,
                              ),
                            ),
                            padding: const EdgeInsets.all(8),
                            child: const Icon(
                              FontAwesomeIcons.phone,
                              size: 20,
                            ))
                      ]))
            ])));
  }
}
