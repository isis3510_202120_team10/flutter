// Copyright 2013 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs
import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/delivery_cubit.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import 'page.dart';

const API_KEY = 'AIzaSyDRs42YEMYntj1_R64Wjzflhrb1AT87Ov0';

// ignore: must_be_immutable
class OrderDetailScreen extends GoogleMapExampleAppPage {
  OrderDetailScreen(this.stateConnection, {key})
      : super(const Icon(Icons.map), 'User interface');
  MyConnectionState stateConnection;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyDeliveryCubit, MyDeliveryState>(
        builder: (context, state) {
      if (state is MyDeliveryCompleteCurrentState) {
        var currentDelivery = state.delivery;
        return MapUiBody(
            currentDelivery: currentDelivery!,
            polylineCoordinates: state.polylineCoordinates,
            stateConnection: stateConnection);
      } else if (state is MyDeliveryErrorState) {
        return Text(state.message);
      } else {
        return CircularProgressIndicator();
      }
    });
  }
}

class MapUiBody extends StatefulWidget {
  final MyDelivery currentDelivery;
  final MyConnectionState stateConnection;
// List of coordinates to join
  final List<LatLng> polylineCoordinates;
  const MapUiBody(
      {required this.currentDelivery,
      required this.stateConnection,
      required this.polylineCoordinates});

  @override
  State<StatefulWidget> createState() =>
      MapUiBodyState(currentDelivery: currentDelivery);
}

class MapUiBodyState extends State<MapUiBody> {
  MyDelivery currentDelivery;
  MyConnectionState get stateConnection => widget.stateConnection;

  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>? deliveryRegister;
  LatLng get fromPoint {
    return LatLng(currentDelivery.deliveryOrigin!.coordinate.latitude,
        currentDelivery.deliveryOrigin!.coordinate.longitude);
  }

  LatLng get toPoint {
    return LatLng(currentDelivery.deliveryDestination!.coordinate.latitude,
        currentDelivery.deliveryDestination!.coordinate.longitude);
  }

  MapUiBodyState({
    required this.currentDelivery,
  });
  bool showMenu = true;
  // Object for PolylinePoints
  late PolylinePoints polylinePoints;

// Map storing polylines created by connecting two points
  Map<PolylineId, Polyline> polylines = {};
  DateTime ultimoMovimiento = DateTime(2000, 1, 1);

  bool showBanner = true;

  void _toggleBanner() {
    setState(() {
      showBanner = !showBanner;
    });
  }

  // Defining latitude and longitude

  static final CameraPosition _kInitialPosition = const CameraPosition(
    target: LatLng(4.6, -72.6),
    zoom: 11.0,
  );

  CameraPosition _position = _kInitialPosition;
  bool _isMapCreated = false;
  final bool _isMoving = false;
  final bool _compassEnabled = true;
  final bool _mapToolbarEnabled = true;
  final CameraTargetBounds _cameraTargetBounds = CameraTargetBounds.unbounded;
  final MinMaxZoomPreference _minMaxZoomPreference = MinMaxZoomPreference.unbounded;
  final MapType _mapType = MapType.normal;
  final bool _rotateGesturesEnabled = true;
  final bool _scrollGesturesEnabled = true;
  final bool _tiltGesturesEnabled = true;
  final bool _zoomControlsEnabled = false;
  final bool _zoomGesturesEnabled = true;
  final bool _indoorViewEnabled = true;
  final bool _myLocationEnabled = false;
  final bool _myTrafficEnabled = false;
  final bool _myLocationButtonEnabled = false;
  late GoogleMapController _controller;
  LatLng currentPosition = LatLng(4.6, -72.2);

  final Location _location = Location();

  String get _getStatusText {
    switch (currentDelivery.status) {
      case 0:
        return 'Picking Up';
      case 1:
        return 'Delivering';
      case 2:
        return 'Finished';
      default:
        return '';
    }
  }

  Color get _getStatusColor {
    switch (currentDelivery.status) {
      case 0:
        return Colors.deepOrange[900]!;
      case 1:
        return Colors.cyan;
      case 2:
        return Colors.pink;
      default:
        return Colors.black;
    }
  }


  @override
  void initState() {
    this.currentPosition = fromPoint;
    super.initState();
    _initLocation();
    _listenDelivery();
  }

  Future<void> _initLocation() async {
    var _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    var _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    // Temporarily disabled since location will be asked when the user is using the particular screen
    // _location.enableBackgroundMode(enable: true);

    _location.changeSettings(interval: 15000);

    this.currentPosition =
        LatLng(fromPoint.latitude - 0.1, fromPoint.longitude);
    CameraUpdate.newLatLngZoom(currentPosition, 11);

    _location.onLocationChanged.listen((LocationData event) {
      if (_controller != null && mounted) {
        this.currentPosition =
            LatLng(fromPoint.latitude - 0.1, fromPoint.longitude);
        final time = DateTime.now().difference(ultimoMovimiento);
        if (time.inSeconds > 30) {
          _controller.animateCamera(
            CameraUpdate.newLatLngZoom(currentPosition, 11),
          );
          ultimoMovimiento = DateTime.now();
        } else {
          setState(() {});
        }
        if (stateConnection is MyConnectionCompleteState) {
          if ((stateConnection as MyConnectionCompleteState).connected) {
            _getRoute(
                startLatitude: fromPoint.latitude,
                startLongitude: fromPoint.longitude,
                destinationLatitude: toPoint.latitude,
                destinationLongitude: toPoint.longitude);
          }
        }
      }
    });
  }

  @override
  void dispose() {
    deliveryRegister?.cancel();
    super.dispose();
  }

  void _setMapStyle(String mapStyle) {
    setState(() {
      _controller.setMapStyle(mapStyle);
    });
  }

  // Should only be called if _isMapCreated is true.

  @override
  Widget build(BuildContext context) {
    _createPolylines();
    if (widget.stateConnection is MyConnectionCompleteState) {
      if ((widget.stateConnection as MyConnectionCompleteState).connected) {
        showBanner = true;
      }
    }
    final GoogleMap googleMap = GoogleMap(
      onMapCreated: onMapCreated,
      markers: _createMarkers(),
      polylines: Set<Polyline>.of(polylines.values),
      initialCameraPosition: _kInitialPosition,
      compassEnabled: _compassEnabled,
      mapToolbarEnabled: _mapToolbarEnabled,
      cameraTargetBounds: _cameraTargetBounds,
      minMaxZoomPreference: _minMaxZoomPreference,
      mapType: _mapType,
      rotateGesturesEnabled: _rotateGesturesEnabled,
      scrollGesturesEnabled: _scrollGesturesEnabled,
      tiltGesturesEnabled: _tiltGesturesEnabled,
      zoomGesturesEnabled: _zoomGesturesEnabled,
      zoomControlsEnabled: _zoomControlsEnabled,
      indoorViewEnabled: _indoorViewEnabled,
      myLocationEnabled: _myLocationEnabled,
      myLocationButtonEnabled: _myLocationButtonEnabled,
      trafficEnabled: _myTrafficEnabled,
      onCameraMove: _updateCameraPosition,
    );
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        googleMap,
        _bottomWidget(context, size),
        if (stateConnection is MyConnectionCompleteState &&
            !(stateConnection as MyConnectionCompleteState).connected &&
            showBanner)
          MaterialBanner(
              padding: const EdgeInsets.all(15),
              content: const Text(
                'You do not have an internet connection at this time. Your data has been temporarily saved until you reconnect. Please do not close the application forcibly to avoid data loss. Route cannot currently be displayed without internet connection.',
              ),
              leading: const Icon(Icons.warning),
              backgroundColor: Colors.amber,
              actions: <Widget>[
                TextButton(
                  child: const Text('DISMISS'),
                  onPressed: () => _toggleBanner(),
                ),
              ])
      ],
    );
  }

  void _updateCameraPosition(CameraPosition position) {
    setState(() {
      _position = position;
    });
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller = controller;
      _isMapCreated = true;
    });
  }

  Set<Marker> _createMarkers() {
    var tmp = Set<Marker>();

    tmp.add(
      Marker(
        markerId: MarkerId('toPoint'),
        position: toPoint,
      ),
    );
    return tmp;
  }

  _createPolylines() {
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Defining an ID
    PolylineId id = PolylineId('poly');

    // Initializing Polyline
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.green.withOpacity(0.65),
      points: widget.polylineCoordinates,
      width: 5,
    );

    // Adding the polyline to the map
    polylines[id] = polyline;
  }

  _getRoute({
    double startLatitude = 0.0,
    double startLongitude = 0.0,
    double destinationLatitude = 0.0,
    double destinationLongitude = 0.0,
  }) async {
    // Generating the list of coordinates to be used for
    // drawing the polylines
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.driving,
    );

    // Adding the coordinates to the list
    final coordinates = result.points
        .map((point) => LatLng(point.latitude, point.longitude))
        .toList();
    if (mounted) {
      await context.read<MyDeliveryCubit>().updateCoordinates(coordinates);
    }
  }

  Widget _bottomWidget(BuildContext context, size) {
    return Positioned(
      bottom: 0,
      child: SizedBox(
        height: 400,
        width: size.width,
        child: Center(
          child: GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(
                  17,
                ),
              ),
              height: 400,
              width: size.width,
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            Text(
                              'Restaurant',
                              style: TextStyle(
                                color: Colors.green[700],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            FutureBuilder<DocumentSnapshot>(
                                future: currentDelivery.restaurantID.get(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    Map<String, dynamic> data = snapshot.data!
                                        .data() as Map<String, dynamic>;
                                    return Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10, top: 5),
                                        child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(150),
                                            child: CachedNetworkImage(
                                              width: size.width * 0.2,
                                              height: 75,
                                              placeholder: (context, url) =>
                                                  CircularProgressIndicator(),
                                              imageUrl: data['logo'] as String,
                                              fit: BoxFit.cover,
                                            )));
                                  }
                                  return const CircularProgressIndicator();
                                }),
                            FutureBuilder<DocumentSnapshot>(
                                future: currentDelivery.restaurantID.get(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    Map<String, dynamic> data = snapshot.data!
                                        .data() as Map<String, dynamic>;
                                    return Text(
                                      data['name'] as String,
                                      style: TextStyle(
                                        color: Colors.grey[800],
                                      ),
                                    );
                                  }
                                  return const CircularProgressIndicator();
                                }),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              'Driver',
                              style: TextStyle(
                                color: Colors.green[700],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            FutureBuilder<DocumentSnapshot>(
                                future: currentDelivery.userID.get(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    Map<String, dynamic> data = snapshot.data!
                                        .data() as Map<String, dynamic>;
                                    if (data['image'] != null) {
                                      return Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10, top: 5),
                                          child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(150),
                                              child: CachedNetworkImage(
                                                width: size.width * 0.2,
                                                height: 75,
                                                placeholder: (context, url) =>
                                                    CircularProgressIndicator(),
                                                imageUrl:
                                                    data['image'] as String,
                                                fit: BoxFit.cover,
                                              )));
                                    }
                                    if (data['image'] == null){
                                      return Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10, top: 5),
                                          child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(150),
                                              child: Image.asset(
                                                'assets/wikiwiki.jpg',
                                                width: size.width * 0.2,
                                                height: 75,
                                                fit: BoxFit.cover,
                                              )));
                                    }
                                  }
                                  return const CircularProgressIndicator();
                                }),
                            FutureBuilder<DocumentSnapshot>(
                                future: currentDelivery.userID.get(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    Map<String, dynamic> data = snapshot.data!
                                        .data() as Map<String, dynamic>;
                                    return Text(
                                      data['name'] + ' ' + data['lastName']
                                          as String,
                                      style: TextStyle(
                                        color: Colors.grey[800],
                                      ),
                                    );
                                  }
                                  return const CircularProgressIndicator();
                                }),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              'Order status',
                              style: TextStyle(
                                color: Colors.green[700],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            if (currentDelivery.status != 2)
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 27.5, bottom: 27.5),
                                child: Container(
                                  height: 25,
                                  width: 25,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: _getStatusColor,
                                      width: 4,
                                    ),
                                  ),
                                ),
                              ),
                            if (currentDelivery.status == 2)
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 27.5, bottom: 27.5),
                                child: Container(
                                  height: 25,
                                  width: 25,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        color: _getStatusColor,
                                        width: 4,
                                      ),
                                      color: _getStatusColor),
                                ),
                              ),
                            Text(
                              _getStatusText,
                              style: TextStyle(
                                color: Colors.grey[800],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10, top: 15),
                        child: Text.rich(
                          TextSpan(
                            style: const TextStyle(
                              fontSize: 15,
                            ),
                            children: [
                              TextSpan(
                                text: 'Order ID: ',
                                style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: currentDelivery.orderID + '\n',
                              ),
                              TextSpan(
                                text: 'Created at: ',
                                style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: currentDelivery.startDateFormated + '\n',
                              ),
                              TextSpan(
                                text: 'Time spent: ',
                                style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                  text: (currentDelivery.finishDate ??
                                              DateTime.now())
                                          .difference(currentDelivery.startDate)
                                          .inMinutes
                                          .toString() +
                                      ' minutes'),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 5, child: Divider(color: Colors.grey)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10, top: 15),
                        child: Text.rich(
                          TextSpan(
                            style: const TextStyle(
                              fontSize: 15,
                            ),
                            children: [
                              TextSpan(
                                text: 'Delivery Origin: ',
                                style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: currentDelivery.deliveryOrigin!.address +
                                    '\n',
                              ),
                              TextSpan(
                                text: 'Delivery Destination: ',
                                style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: currentDelivery
                                    .deliveryDestination!.address,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 5, child: Divider(color: Colors.grey)),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10, top: 15),
                        child: Text.rich(
                          TextSpan(
                            style: const TextStyle(
                              fontSize: 15,
                            ),
                            children: [
                              TextSpan(
                                text: 'Proof of Delivery Image: ',
                                style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Missing to connect to delivery image. Future functionality
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 5),
                      child: Image.asset('assets/placeholder-image.png',
                          width: size.width * 0.30),
                    ),
                  ],
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  void _listenDelivery() async {
    context.read<MyDeliveryCubit>().listenDelivery(
        currentDelivery,
        (deliveryData) => setState(() {
              currentDelivery = deliveryData;
            }));
  }

  Future _changeStatus() async {
    await context.read<MyDeliveryCubit>().updateDelivery(currentDelivery);
  }
}
