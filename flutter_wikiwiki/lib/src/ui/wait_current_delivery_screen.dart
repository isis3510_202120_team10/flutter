import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/wait_time_cubit.dart';
import 'package:flutter_wikiwiki/src/navigation/routes.dart';
import 'package:flutter_wikiwiki/src/repository/wait_time_repository.dart';

class WaitCurrentDeliveryScreen extends StatefulWidget {
  const WaitCurrentDeliveryScreen({Key? key}) : super(key: key);
  static Widget create(BuildContext context) {
    return BlocProvider<WaitTimeCubit>(
      create: (_) {
        final repository = context.read<WaitTimeRepositoryBase>();
        return WaitTimeCubit(repository);
      },
      child: const WaitCurrentDeliveryScreen(),
    );
  }

  @override
  _WaitCurrentDeliveryState createState() => _WaitCurrentDeliveryState();
}

class _WaitCurrentDeliveryState extends State<WaitCurrentDeliveryScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectionCubit, MyConnectionState>(
        buildWhen: (previous, current) => current is AuthSignedIn,
        builder: (context, stateConnection) {
          return BlocBuilder<AuthCubit, AuthState>(
              builder: (context, state) {
                final userId = (state as AuthSignedIn).user.uid;
            if (stateConnection is MyConnectionCompleteState) {
              return BlocBuilder<WaitTimeCubit, WaitTimeState>(
                  builder: (context, state) {
                if (state is WaitTimeInitialState) {

                  if (!stateConnection.connected) {
                    context.read<WaitTimeCubit>().getWaitTimeLocal(userId);
                  } else {
                    context.read<WaitTimeCubit>().getWaitTimeRemote(userId);
                  }
                }
                if (state is WaitTimeCompleteState) {
                  final time = state.time;
                  final extra = (time == 'failed')
                      ? '.'
                      : '**for ${time} minutes approximate**';
                  final List<String> texts = [
                    "You don't have any deliveries in progress.",
                    'You can sit back, relax and enjoy life $extra',
                    "We'll notify you when a new delivery is assigned to you 😎"
                  ];
                  return Scaffold(
                      body: Center(
                          child: SingleChildScrollView(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                        Image.asset(
                          'assets/relaxing.png',
                          width: 300,
                        ),
                        const SizedBox(height: 8),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: texts.length,
                          itemBuilder: (_, int index) {
                            return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    texts[index],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyText2
                                            ?.color),
                                  ),
                                  const SizedBox(height: 25)
                                ]);
                          },
                        ),
                        Image.asset(
                          'assets/qr-form.png',
                          width: 80,
                        ),
                        const SizedBox(height: 8),
                        SizedBox(
                            width: 300,
                            height: 40,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ))),
                              child: const Text('See previous deliveries'),
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacementNamed(Routes.previous);
                              },
                            ))
                      ]))));
                }
                return Center( child: CircularProgressIndicator());
              });
            }
            return Center( child: CircularProgressIndicator());
          });
        });
  }
}
