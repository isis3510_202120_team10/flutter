import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/deliveries_cubit.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';
import 'package:flutter_wikiwiki/src/ui/utils/no_connection_toastr.dart';
import 'package:flutter_wikiwiki/src/ui/utils/search_widget.dart';

class PreviousScreen extends StatefulWidget {
  const PreviousScreen({Key? key}) : super(key: key);
  @override
  _PreviousScreenState createState() => _PreviousScreenState();

  static Widget create(BuildContext context, bool isowner) {
    if (!isowner) {
      return BlocProvider<MyDeliveriesCubit>(
        create: (_) {
          final repository = context.read<MyDeliveryRepositoryBase>();
          return MyDeliveriesCubit(repository)..loadDeliveriesFromMe(2);
        },
        child: const PreviousScreen(),
      );
    } else {
      return BlocProvider<MyDeliveriesCubit>(
        create: (_) {
          final repository = context.read<MyDeliveryRepositoryBase>();
          return MyDeliveriesCubit(repository)..loadDeliveriesByStatus(2);
        },
        child: const PreviousScreen(),
      );
    }
  }
}

class _PreviousScreenState extends State<PreviousScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectionCubit, MyConnectionState>(
        builder: (context, stateConnection) {
      if (stateConnection is MyConnectionCompleteState &&
          !stateConnection.connected) {
        toastMessage(context);
      }
      return BlocBuilder<AuthCubit, AuthState>(
          buildWhen: (previous, current) => current is AuthSignedIn,
          builder: (context, state) {
            return BlocBuilder<MyDeliveriesCubit, MyDeliveriesState>(
                builder: (context, state) {
              if (state is MyDeliveriesLoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is MyDeliveriesErrorState) {
                return Text(state.message);
              } else if (state is MyDeliveriesCompleteState) {
                final deliveries = state.deliveries;
                return PreviousCompleteScreen(deliveries: deliveries);
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            });
          });
    });
  }
}

class PreviousCompleteScreen extends StatefulWidget {
  final List<MyDelivery> deliveries;
  const PreviousCompleteScreen({required this.deliveries});

  @override
  _PreviousCompleteScreenState createState() =>
      _PreviousCompleteScreenState(deliveries: deliveries);
}

class _PreviousCompleteScreenState extends State<PreviousCompleteScreen> {
  List<MyDelivery> deliveries;
  late List<MyDelivery> filterDeliveries;
  String query = '';
  _PreviousCompleteScreenState({required this.deliveries});

  @override
  void initState() {
    super.initState();
    filterDeliveries = deliveries;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: buildSearch(),
        elevation: 0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: ListView.builder(
          itemCount: filterDeliveries.length,
          itemBuilder: (_, int index) {
            return _ListItem(delivery: filterDeliveries[index]);
          }),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Search',
        onChanged: searchDelivery,
      );

  void searchDelivery(String query) {
    final searchLower = query.toLowerCase();
    final filterDeliveries = deliveries.where((delivery) {
      final name = delivery.clientName.toLowerCase();
      final startDate = delivery.startDateFormated.toLowerCase();
      final timeSpend = delivery.finishDate
          ?.difference(delivery.startDate)
          .inMinutes
          .toString()
          .toLowerCase() as String;
      return startDate.contains(searchLower) ||
          timeSpend.contains(searchLower) ||
          name.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.filterDeliveries = filterDeliveries;
    });
  }
}

class _ListItem extends StatelessWidget {
  final MyDelivery delivery;

  const _ListItem({
    Key? key,
    required this.delivery,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final startDate = delivery.startDateFormated;
    final timeSpend = delivery.finishDate
        ?.difference(delivery.startDate)
        .inMinutes
        .toString() as String;
    final color = Theme.of(context).colorScheme.primary;
    final styleTitle = TextStyle(color: color, fontWeight: FontWeight.bold);

    return Center(
      child: Card(
        elevation: 5,
        margin: const EdgeInsets.all(15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Row(children: <Widget>[
        Padding(
        padding: const EdgeInsets.all(15.0),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(150),
              child: CachedNetworkImage(
                width: 75,
                height: 75,
                placeholder: (context, url) =>
                    CircularProgressIndicator(),
                imageUrl: delivery.restaurant!.logo as String,
                fit: BoxFit.cover,
              ))),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10.0, 0, 10.0),
            child:
                Wrap(direction: Axis.vertical, spacing: 15, children: <Widget>[
              Row(children: <Widget>[
                Icon(Icons.calendar_today, color: color, size: 15),
                Text(' Date: ', style: styleTitle),
                Text(startDate)
              ]),
              Row(children: <Widget>[
                Icon(Icons.person, color: color, size: 15),
                Text(' Client: ', style: styleTitle),
                Text(delivery.clientName)
              ]),
              Row(children: <Widget>[
                Icon(Icons.access_time, color: color, size: 15),
                Text(' Time spent: ', style: styleTitle),
                Text(timeSpend + ' min')
              ]),
            ]),
          ),
        ]),
      ),
    );
  }
}
