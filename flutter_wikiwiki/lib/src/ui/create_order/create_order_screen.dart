import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/my_user_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/orders_owner_cubit.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/model/user.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_delivery_repository.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CreateOrderScreen extends StatefulWidget {
  final MyDelivery? myDelivery;
  final bool isSaving;
  final List<MyUser> deliveryGuys;

  const CreateOrderScreen(
      {Key? key,
      this.myDelivery,
      this.deliveryGuys = const [],
      this.isSaving = false})
      : super(key: key);

  static Widget create(BuildContext context) {
    return BlocProvider(
        create: (_) => OrdersOwnerCubit(MyDeliveryRepository())..init(),
        child: BlocBuilder<OrdersOwnerCubit, OrdersOwnerState>(
            builder: (context, state) {
          if (state is OrdersOwnerReadyState) {
            return CreateOrderScreen(
                myDelivery: state.delivery,
                isSaving: state.isSaving,
                deliveryGuys: state.deliveryGuys);
          }
          return const Center(child: CircularProgressIndicator());
        }));
  }

  @override
  _CreateOrderScreenState createState() => _CreateOrderScreenState();
}

class _CreateOrderScreenState extends State<CreateOrderScreen> {
  // @override
  // void initState() {
  //   super.initState();
  //   _listenDeliveriesOwner();
  // }

  final _formKey = GlobalKey<FormState>();
  final _clientNameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _addressController = TextEditingController();
  final _latitudeController = TextEditingController();
  final _longitudeController = TextEditingController();
  final _deliveryPriceController = TextEditingController();

  String? stringValidator(String? value) {
    return (value == null || value.isEmpty) ? 'This is a required field' : null;
  }

  @override
  void initState() {
    _clientNameController.text = widget.myDelivery?.clientName ?? '';
    _phoneController.text = widget.myDelivery?.phone ?? '';
    _addressController.text =
        widget.myDelivery?.deliveryDestination!.address ?? '';
    _latitudeController.text =
        (widget.myDelivery?.deliveryDestination!.coordinate.latitude ?? '')
            .toString();
    _longitudeController.text =
        (widget.myDelivery?.deliveryDestination!.coordinate.longitude ?? '')
            .toString();
    _deliveryPriceController.text =
        (widget.myDelivery?.deliveryPrice ?? '').toString();
    super.initState();
  }

  void resetFields() {
    _clientNameController.text = '';
    _phoneController.text = '';
    _addressController.text = '';
    _latitudeController.text =
        (widget.myDelivery?.deliveryDestination!.coordinate.latitude ?? '')
            .toString();
    _longitudeController.text =
        (widget.myDelivery?.deliveryDestination!.coordinate.latitude ?? '')
            .toString();
    _deliveryPriceController.text =
        (widget.myDelivery?.deliveryPrice ?? '').toString();
  }

  bool showBanner = true;

  void _toggleBanner() {
    setState(() {
      showBanner = !showBanner;
    });
  }

  bool isChecked = false;
  String dropdownValue = '';

  @override
  Widget build(BuildContext context) {
    if (dropdownValue == '') {
      dropdownValue = widget.deliveryGuys[0].id;
    }
    return BlocBuilder<AuthCubit, AuthState>(
        buildWhen: (previous, current) => current is AuthSignedIn,
        builder: (context, state) {
          return BlocBuilder<OrdersOwnerCubit, OrdersOwnerState>(
              builder: (context, state) {
            return SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(16),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Client information',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.headline6!.color,
                            fontWeight: FontWeight.bold,
                          )),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 6.0),
                              child: TextFormField(
                                  controller: _clientNameController,
                                  decoration:
                                      const InputDecoration(labelText: 'Name'),
                                  validator: stringValidator),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 6.0),
                              child: TextFormField(
                                  controller: _phoneController,
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration(
                                      labelText: 'Phone number'),
                                  validator: stringValidator),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      Text('Delivery destination',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.headline6!.color,
                            fontWeight: FontWeight.bold,
                          )),
                      TextFormField(
                          controller: _addressController,
                          decoration:
                              const InputDecoration(labelText: 'Address'),
                          validator: stringValidator),
                      const SizedBox(height: 8),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 6.0),
                              child: TextField(
                                controller: _latitudeController,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                    labelText: 'Latitude'),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 6.0),
                              child: TextField(
                                controller: _longitudeController,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                    labelText: 'Longitude'),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      Text('Complementary info',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.headline6!.color,
                            fontWeight: FontWeight.bold,
                          )),
                      const SizedBox(height: 16),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          const Text('Will pay on the delivery?'),
                          Checkbox(
                            checkColor: Colors.white,
                            value: isChecked,
                            onChanged: (bool? value) {
                              setState(() {
                                isChecked = value!;
                              });
                            },
                          )
                        ],
                      ),
                      TextField(
                        controller: _deliveryPriceController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            labelText: 'Delivery price', prefixText: '\$'),
                      ),
                      const SizedBox(height: 16),
                      Text('Delivery guy',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.headline6!.color,
                            fontWeight: FontWeight.bold,
                          )),
                      DropdownButton<String>(
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(color: Colors.deepPurple),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            dropdownValue = newValue!;
                          });
                        },
                        items: widget.deliveryGuys
                            .map<DropdownMenuItem<String>>((MyUser myUser) {
                          return DropdownMenuItem<String>(
                            value: myUser.id,
                            child: Text(myUser.name + ' ' + myUser.lastName),
                          );
                        }).toList(),
                      ),
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          ElevatedButton.icon(
                            label: const Text('Create'),
                            icon: const Icon(Icons.add, size: 18),
                            onPressed: widget.isSaving
                                ? null
                                : () async {
                                    if (_formKey.currentState?.validate() ==
                                        true) {
                                      final Delivery destination = Delivery(
                                          _addressController.text,
                                          GeoPoint(
                                              _latitudeController.text == ''
                                                  ? 4.710493684827736
                                                  : double.parse(
                                                      _latitudeController.text),
                                              _longitudeController.text == ''
                                                  ? -74.04221545820411
                                                  : double.parse(
                                                      _longitudeController
                                                          .text)));
                                      final double delPrice =
                                          _deliveryPriceController.text == ''
                                              ? 0
                                              : double.parse(
                                                  _deliveryPriceController
                                                      .text);
                                      const Delivery origin = const Delivery(
                                          'Cl. 93 #12-41, Bogotá',
                                          GeoPoint(4.673454, -74.043767));
                                      final int randomNumber =
                                          Random().nextInt(9999);
                                      final DocumentReference? restaurant =
                                          (context.read<MyUserCubit>().state
                                                  as MyUserReadyState)
                                              .user
                                              .restaurant;
                                      await context
                                          .read<OrdersOwnerCubit>()
                                          .saveMyDelivery(
                                              '',
                                              _clientNameController.text,
                                              destination,
                                              origin,
                                              randomNumber.toString(),
                                              _phoneController.text,
                                              DateTime.now(),
                                              0,
                                              isChecked,
                                              restaurant!.id,
                                              dropdownValue,
                                              DateTime.now().toIso8601String(),
                                              delPrice);
                                      resetFields();
                                      Fluttertoast.showToast(
                                          msg: 'Order created correctly',
                                          toastLength: Toast.LENGTH_LONG,
                                          gravity: ToastGravity.BOTTOM,
                                          backgroundColor: Colors.green,
                                          textColor: Colors.white,
                                          fontSize: 12.0);
                                    }
                                  },
                          ),
                        ],
                      ),
                      const SizedBox(height: 8),
                      BlocBuilder<ConnectionCubit, MyConnectionState>(
                        builder: (context, connectionState) {
                          if (widget.isSaving &&
                              connectionState is MyConnectionCompleteState &&
                              !connectionState.connected &&
                              showBanner) {
                            return MaterialBanner(
                                padding: const EdgeInsets.all(15),
                                content: const Text(
                                  'You do not have an internet connection at this time. The order has been temporarily saved until you reconnect. Please do not close the application forcibly to avoid data loss.',
                                ),
                                leading: const Icon(Icons.warning),
                                backgroundColor: Colors.amber,
                                actions: <Widget>[
                                  TextButton(
                                    child: const Text('DISMISS'),
                                    onPressed: () => _toggleBanner(),
                                  ),
                                ]);
                          }
                          return Container();
                        },
                      )
                    ],
                  ),
                ),
              ),
            );
          });
        });
  }

  // void _listenDeliveriesOwner() async {
  //   MyUser user = (context.read<MyUserCubit>().state as MyUserReadyState).user;
  //   context.read<OrdersOwnerCubit>().listenDeliveriesOwner(
  //       (deliveryData) => setState(() {
  //             deliveries = deliveryData;
  //           }),
  //       user);
  // }
}
