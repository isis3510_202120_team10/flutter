import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/delivery_cubit.dart';
import 'package:flutter_wikiwiki/src/model/delivery.dart';
import 'package:flutter_wikiwiki/src/repository/my_delivery_repository.dart';
import 'package:flutter_wikiwiki/src/ui/utils/no_connection_toastr.dart';
import 'package:flutter_wikiwiki/src/ui/wait_current_delivery_screen.dart';

import 'current_delivery_screen.dart';

MyDeliveryCubit _deliveryCubit = MyDeliveryCubit();

class JoinCurrentScreen extends StatefulWidget {
  const JoinCurrentScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) {
    return BlocProvider<MyDeliveryCubit>(
      create: (_) {
        final repository = context.read<MyDeliveryRepositoryBase>();
        _deliveryCubit.deliveryRepository = repository;
        return _deliveryCubit..loadCurrentDelivery();
      },
      child: const JoinCurrentScreen(),
    );
  }

  @override
  _JoinCurrentScreenState createState() => _JoinCurrentScreenState();
}

class _JoinCurrentScreenState extends State<JoinCurrentScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectionCubit, MyConnectionState>(
        builder: (context, stateConnection) {
      if (stateConnection is MyConnectionCompleteState &&
          !stateConnection.connected) {
        toastMessage(context);
      }
      return BlocBuilder<AuthCubit, AuthState>(
          buildWhen: (previous, current) => current is AuthSignedIn,
          builder: (context, state) {
            return BlocBuilder<MyDeliveryCubit, MyDeliveryState>(
                builder: (context, state) {
              if (state is MyDeliveryLoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is MyDeliveryErrorState) {
                return Text(state.message);
              } else if (state is MyDeliveryCompleteCurrentState) {
                return JoinCurrentCompleteScreen(
                    delivery: state.delivery, stateConnection: stateConnection);
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            });
          });
    });
  }
}

class JoinCurrentCompleteScreen extends StatefulWidget {
  final MyDelivery? delivery;
  final MyConnectionState stateConnection;
  const JoinCurrentCompleteScreen(
      {required this.delivery, required this.stateConnection});

  @override
  _JoinCurrentCompleteScreenState createState() {
    return _JoinCurrentCompleteScreenState(
      delivery: delivery,
    );
  }
}

class _JoinCurrentCompleteScreenState extends State<JoinCurrentCompleteScreen> {
  MyDelivery? delivery;

  _JoinCurrentCompleteScreenState({
    required this.delivery,
  });

  @override
  void initState() {
    super.initState();
    _listenDelivery();
  }

  @override
  Widget build(BuildContext context) {
    if (delivery != null) {
      return CurrentDeliveryScreen(widget.stateConnection);
    } else {
      return WaitCurrentDeliveryScreen.create(context);
    }
  }

  void _listenDelivery() async {
    context.read<MyDeliveryCubit>().listenCurrentDelivery((deliveryData) {
      context.read<MyDeliveryCubit>().emitCurrent(deliveryData);
      setState(() {
        delivery = deliveryData;
      });
    });
  }
}
