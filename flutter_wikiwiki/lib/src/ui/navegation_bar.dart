import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/my_user_cubit.dart';
import 'package:flutter_wikiwiki/src/repository/implementations/my_user_repository.dart';
import 'package:flutter_wikiwiki/src/ui/previous_delivery_screen.dart';
import 'package:flutter_wikiwiki/src/ui/profile/profile_screen.dart';
import 'package:flutter_wikiwiki/src/ui/profile/profile_screen_edit.dart';

import 'join_current_delivery_screen.dart';

/// This is the stateful widget that the main application instantiates.
class NavigationBar extends StatefulWidget {
  int page;
  int selectedProfile = 0;

  NavigationBar({Key? key, required this.page}) : super(key: key);

  static Widget create(BuildContext context, int new_page) {
    return BlocProvider(
      create: (_) => MyUserCubit(MyUserRepository())..getMyUser(),
      child: NavigationBar(page: new_page),
    );
  }

  @override
  State<NavigationBar> createState() => _NavigationBarState(
      selectedIndex: page, selectedProfile: selectedProfile);
}

/// This is the private State class that goes with MyStatefulWidget.
class _NavigationBarState extends State<NavigationBar> {
  int selectedIndex;
  int selectedProfile;
  _NavigationBarState(
      {required this.selectedIndex, required this.selectedProfile})
      : super();

  Future _onItemTapped(int index) async {
    setState(() {
      selectedIndex = index;
    });
  }

  Future _onEditProfileTapped(int index) async {
    setState(() {
      selectedProfile = index;
    });
  }

  Widget _getPage(BuildContext context) {
    switch (selectedIndex) {
      case 0:
        setState(() {
          selectedProfile = 0;
        });
        return JoinCurrentScreen.create(context);
      case 1:
        setState(() {
          selectedProfile = 0;
        });
        return PreviousScreen.create(context, false);
      case 2:
        return ProfileScreen(screen: selectedProfile);
      default:
        return const Scaffold(body: Text("Not Found"));
    }
  }

  @override
  Widget build(BuildContext context) {
    context.read<MyUserCubit>().getMyUser();
    const List<String> titles = [
      "Current delivery",
      "Previous deliveries",
      "Profile",
      "Profile edit",
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(titles[selectedIndex]),
        centerTitle: true,
        actions: selectedIndex == 2
            ? <Widget>[
                TextButton(
                  onPressed: () {
                    _onEditProfileTapped(selectedProfile == 0 ? 1 : 0);
                  },
                  child: const Text(
                    "Edit",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ]
            : <Widget>[],
        elevation: 0,
      ),
      body: _getPage(context),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag),
            label: 'Current Delivery',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            label: 'Previous Deliveries',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        onTap: _onItemTapped,
      ),
    );
  }
}
