import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_wikiwiki/src/bloc/auth_cubit.dart';
import 'package:flutter_wikiwiki/src/bloc/connection_cubit.dart';
import 'package:flutter_wikiwiki/src/provider/sharepreferences.dart';
import 'package:flutter_wikiwiki/src/ui/utils/no_connection.dart';
import 'package:sign_button/sign_button.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({Key? key}) : super(key: key);

  static Widget create(BuildContext context) => const IntroScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Wikiwiki'),
        centerTitle: true,
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: const AssetImage('assets/map.jpg'),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.1), BlendMode.dstATop),
            ),
          ),
          child: _IntroPager()),
    );
  }
}

class _IntroPager extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final isSigningIn = context.watch<AuthCubit>().state is AuthSigningIn;
    return AbsorbPointer(absorbing: isSigningIn, child: _LoginPage());
  }
}

class _LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final isSigningIn = context.watch<AuthCubit>().state is AuthSigningIn;

    final authCubit = context.watch<AuthCubit>();

    return BlocBuilder<ConnectionCubit, MyConnectionState>(
        builder: (context, connectionState) {
      return Center(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 24),
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/wikiwikilogo.png',
                      width: 150,
                      height: 150,
                    ),
                    const SizedBox(height: 8),
                    const Text(
                      'Welcome back',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 24.0, fontWeight: FontWeight.bold),
                    ),
                    if (isSigningIn) const CircularProgressIndicator(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Column(
                        children: [
                          const _EmailSignInOrUp(),
                          const SizedBox(height: 8),
                          const Text(
                            'OR',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12.0, fontWeight: FontWeight.bold),
                          ),
                          SignInButton(
                              buttonType: ButtonType.google,
                              onPressed: () {
                                if (connectionState
                                        is MyConnectionCompleteState &&
                                    !connectionState.connected) {
                                  connectionFailedMessage(
                                      context,
                                      'Connection failed',
                                      'Check your internet connection and try logging in with Google again.');
                                } else {
                                  authCubit.signInWithGoogle();
                                }
                              }),
                          const SizedBox(height: 8),
                          RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text:
                                    'By proceeding, you also agree to our terms of service. ',
                                style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .color),
                              )),
                          TextButton(
                            child: const Text('Terms of service',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                )),
                            onPressed: () => connectionFailedMessage(
                                context, 'Terms of service', 'Coming soon!'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class _EmailSignInOrUp extends StatefulWidget {
  const _EmailSignInOrUp({Key? key}) : super(key: key);

  @override
  _EmailSignInOrUpState createState() => _EmailSignInOrUpState();
}

class _EmailSignInOrUpState extends State<_EmailSignInOrUp> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  String? emailValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'This is a required field';
    }
    if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(value) ==
        false) {
      return 'The email is not complete. Please check your writing';
    }
    return null;
  }

  String? passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'This is a required field';
    }
    if (value.length < 6) {
      return 'Password should be at least 6 characters';
    }
    return null;
  }

  int _failedAttempts = 0;
  static const int maxAttemptsTotal = 5;
  final providerLocal = SharePreferencesProvider();

  @override
  void initState() {
    super.initState();
    providerLocal.getLockAttempts().then((attempts) {
      setState(() {
        _failedAttempts = attempts % maxAttemptsTotal;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (_, state) {
        return BlocBuilder<ConnectionCubit, MyConnectionState>(
          builder: (context, connectionState) {
            return Form(
              key: _formKey,
              child: Column(
                children: [
                  const SizedBox(height: 10),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0)),
                      filled: true,
                      isDense: true,
                      fillColor: Theme.of(context).colorScheme.onBackground,
                      prefixIcon: const Icon(Icons.email_rounded),
                    ),
                    validator: emailValidator,
                  ),
                  const SizedBox(height: 8),
                  TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                        hintText: 'Password',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        filled: true,
                        isDense: true,
                        fillColor: Theme.of(context).colorScheme.onBackground,
                        prefixIcon: const Icon(Icons.lock),
                        suffixIcon: GestureDetector(
                          onTap: () => _toggle(),
                          child: Icon(_obscureText
                              ? Icons.visibility
                              : Icons.visibility_off),
                        )),
                    validator: passwordValidator,
                    obscureText: _obscureText,
                  ),
                  const SizedBox(height: 8),
                  if (state is AuthError)
                    Text(
                      state.message,
                      style: const TextStyle(color: Colors.red, fontSize: 12),
                    ),
                  const SizedBox(height: 20),
                  _LoginButton(
                    text: 'Log in',
                    color: Theme.of(context).colorScheme.primary,
                    textColor: Colors.white,
                    onTap: () async {
                      if (_formKey.currentState?.validate() == true) {
                        await context.read<AuthCubit>().verifyUnlock();
                        if (connectionState is MyConnectionCompleteState &&
                            !connectionState.connected) {
                          connectionFailedMessage(context, 'Connection failed',
                              'Check your internet connection and try logging in again.');
                        } else if (await providerLocal.shouldLock()) {
                          final DateTime? date =
                              await providerLocal.getLockDate();
                          int diff = date!.difference(DateTime.now()).inSeconds;
                          String unit = diff == 1 ? ' second' : ' seconds';
                          if (diff > 60) {
                            diff = date.difference(DateTime.now()).inMinutes;
                            unit = diff == 1 ? ' minute' : ' minutes';
                          }
                          connectionFailedMessage(
                              context,
                              'Error',
                              'Too many attemps. Please wait ' +
                                  diff.toString() +
                                  unit);
                        } else {
                          await providerLocal.incrementLockAttempts();
                          _failedAttempts++;
                          if ((_failedAttempts % maxAttemptsTotal) == 0) {
                            await providerLocal.updateLockDate();
                          }
                          context.read<AuthCubit>().signInWithEmailAndPassword(
                                _emailController.text,
                                _passwordController.text,
                              );
                        }
                      }
                    },
                  ),
                  const SizedBox(height: 8),
                  _LoginButton(
                    text: 'Sign up',
                    color: const Color(0xff007aff),
                    textColor: Theme.of(context).textTheme.bodyText1?.color,
                    onTap: () async {
                      if (_formKey.currentState?.validate() == true) {
                        if (connectionState is MyConnectionCompleteState &&
                            !connectionState.connected) {
                          connectionFailedMessage(context, 'Connection failed',
                              'Check your internet connection and try to sign up again.');
                        } else {
                          context
                              .read<AuthCubit>()
                              .createUserWithEmailAndPassword(
                                _emailController.text,
                                _passwordController.text,
                              );
                        }
                      }
                    },
                  )
                ],
              ),
            );
          },
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  final String text;
  final String? imagePath;
  final VoidCallback? onTap;
  final Color color;
  final Color? textColor;

  const _LoginButton({
    Key? key,
    required this.text,
    this.imagePath,
    this.onTap,
    this.color = Colors.greenAccent,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      elevation: 3,
      borderRadius: const BorderRadius.all(Radius.circular(15)),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              if (imagePath != null)
                Image.asset(
                  imagePath!,
                  width: 24,
                  height: 24,
                ),
              const SizedBox(width: 10),
              Expanded(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: textColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
