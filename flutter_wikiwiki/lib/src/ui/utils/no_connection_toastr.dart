import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<void> toastMessage(BuildContext context) async {
  Fluttertoast.showToast(
    msg: 'Network unavailable: check your network. Using offline mode.',
    toastLength: Toast.LENGTH_LONG,
  );
}
