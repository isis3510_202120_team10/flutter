import 'package:flutter/material.dart';

Future<void> connectionFailedMessage(
    BuildContext context, String titleDialog, String message) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Column(children: [
          Image.asset(
            'assets/wikiwiki.jpg',
            width: 100,
            height: 100,
            fit: BoxFit.contain,
          ),
          Text(titleDialog)
        ]),
        content: Text(
          message,
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('OK'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
