import 'dart:ui';

const bool kUseGoogleASignIn = false;
const bool kUseFirebaseAnalytics = true;

const String kAppName = 'WikiWiki';
const String kAppAssetLogo = 'assets/wikiwikilogo.png';

const int kMinPasswordLength = 8;
const double kMinPasswordStrength = 0.5;

const List<Locale> kSupportedLanguages = <Locale>[
  Locale('en', 'US'),
  Locale('fr', 'FR')
];
